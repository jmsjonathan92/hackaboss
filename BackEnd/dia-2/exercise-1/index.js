/**
 * ################
 * ## Exercise 1 ##
 * ################
 *
 * Crea un módulo index que simplemente muestre por pantalla la
 * primera palabra que se le pasa como primer argumento cuando
 * se ejecuta este módulo desde bash.
 *
 */

const firstWord = process.argv[2];

console.log(firstWord);
