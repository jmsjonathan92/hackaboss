module.exports = {
    suma: {
        es: 'sumar',
        en: 'adding',
    },
    resta: {
        es: 'restar',
        en: 'subtracting',
    },
    multiplica: {
        es: 'multiplicar',
        en: 'multiplying',
    },
    divide: {
        es: 'dividir',
        en: 'dividing',
    },
};
