/**
 * ################
 * ## Calculator ##
 * ################
 *
 * Crea una calculadora que reciba 3 argumentos: tipo de operación,
 * número A y número B.
 *
 *      - node calculadora.js resta 100 33
 *
 *      - node calculadora.js divide 100 33
 *
 * A mayores debe recibir la variable de entorno LANG. En función de
 * LANG se mostrará un resultado en un idioma diferente:
 *
 *      - Para: LANG=es node calculadora suma 10 20
 *      - Salida: El resultado de sumar 10 y 20 es: 30
 *
 *      - Para: LANG=en node calculadora suma 10 20
 *      - Salida: The result of adding 10 and 20 is: 30
 *
 */

const chalk = require('chalk');

const { sum, sub, mult, div } = require('./lib/operations');
const translations = require('./lib/translations');

const validOperations = ['suma', 'resta', 'multiplica', 'divide'];

// Obtenemos el idioma.
let language = process.env.LANG;

// Comprobamos que el idioma sea válido. Si no lo es lanzamos
// un error.
if (language !== 'en' && language !== 'es') {
    console.error(chalk.red('Idioma incorrecto'));
    process.exit(1); // El código 1 indica que algo ha ido mal.
}

// Obtenemos la operación y los números mediante destructuring.
const args = process.argv.slice(2);
const [operation, numA, numB] = args;

// Si falta algún argumento del programa lanzamos un error.
if (!operation || !numA || !numB) {
    console.error(chalk.red('Faltan argumentos'));
    process.exit(1);
}

// Si el tipo de operación no es correcto lanzamos un error.
if (!validOperations.includes(operation)) {
    console.error(chalk.red('Tipo de operación incorrecto.'));
    process.exit(1);
}

// Convertimos los números a tipo Number.
const a = Number(numA);
const b = Number(numB);

// Si algún número no es de tipo Number lanzamos un error.
if (isNaN(a) || isNaN(b)) {
    console.error(chalk.red('Debes introducir valores numéricos.'));
    process.exit(1);
}

let result;

switch (operation) {
    case 'suma':
        result = sum(a, b);
        break;

    case 'resta':
        result = sub(a, b);
        break;

    case 'multiplica':
        result = mult(a, b);
        break;

    case 'divide':
        result = div(a, b);
        break;

    default:
        console.error(chalk.red('Ha ocurrido un error inesperado'));
        process.exit(1);
}

// Comprobamos que el resultado no sea indeterminado.
if (isNaN(result)) {
    console.error(chalk.red('El resultado es indeterminado'));
    process.exit(1);
}

// Comprobamos si el resultado es infinito.
if (!isFinite(result)) {
    console.error('El resultado es infinito');
    process.exit(1);
}

// Mostramos el mensaje con el resultado.
if (language === 'es') {
    console.log(
        chalk.green(
            `El resultado de ${translations[operation][language]} ${a} y ${b} es: ${result}`
        )
    );
} else {
    console.log(
        chalk.green(
            `The result of ${translations[operation][language]} ${a} and ${b} is: ${result}`
        )
    );
}

// Terminamos el proceso con el código 0 que indica que todo ha ido de perlas.
process.exit(0);
