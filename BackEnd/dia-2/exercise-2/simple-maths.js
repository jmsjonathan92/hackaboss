/**
 * ################
 * ## Exercise 2 ##
 * ################
 *
 * Crea un módulo "simple-maths" que exponga una función "double(x)"", que calcula
 * el doble de un número que se le pasa como argumento.
 *
 */

const double = (num) => num * 2;

module.exports = double;
