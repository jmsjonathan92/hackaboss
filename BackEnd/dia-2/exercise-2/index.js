/**
 * ################
 * ## Exercise 2 ##
 * ################
 *
 * Crea otro módulo "index" que hace uso de "simple-maths". Este módulo usará como
 * entrada el primer argumento que se le pasa a la ejecución del módulo, y se lo
 * pasará al módulo "simple-maths", quien calculará el doble del número, e "index"
 * se encargará de imprimir el resultado por pantalla.
 *
 */

const double = require('./simple-maths');

const num = Number(process.argv[2]);

const result = double(num);

console.log(result);
