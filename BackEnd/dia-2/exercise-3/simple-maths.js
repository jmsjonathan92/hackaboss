/**
 * ################
 * ## Exercise 3 ##
 * ################
 *
 * Modifica el ejercicio anterior para que cuando el argumento que le
 * llega a la función "double(x)"" del módulo "simple-maths" no sea un número,
 * lance un error.
 *
 */

const double = (num) => {
    if (Number.isNaN(num)) throw new Error('Por favor, inserte un número');

    return num * 2;
};

module.exports = double;
