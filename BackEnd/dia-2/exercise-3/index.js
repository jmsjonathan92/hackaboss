/**
 * ################
 * ## Exercise 3 ##
 * ################
 *
 * Modifica el módulo index para capturar el error y mostrarle al usuario
 * el mensaje: "El argumento debe ser un número por pantalla".
 *
 */

const double = require('./simple-maths');

const num = Number(process.argv[2]);

try {
    const result = double(num);

    console.log(result);
} catch (error) {
    console.log(error.message);
}
