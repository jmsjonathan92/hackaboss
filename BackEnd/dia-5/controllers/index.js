const { html } = require('../components');

const guestBook = [
    {
        name: 'Manolo',
        message: 'Este HTML es brutal!',
    },
    {
        name: 'Lucía',
        message: 'Tu contenido mola mucho.',
    },
];

/**
 * ##########
 * ## Home ##
 * ##########
 */

function home(request, response) {
    response.statusCode = 200;

    response.setHeader('Content-type', 'text/html');

    const guestBookList = guestBook
        .map((item) => {
            return `
            <li>
                <p>${item.message}</p>
                <p><strong>${item.name}</strong></p>
            </li>
        `;
        })
        .join('');

    response.end(
        html({
            title: 'Bienvenid@s a mi web',
            content: `
                <h2>Visitas anteriores</h2>

                <ul>${guestBookList}</ul>

                <h2>Firma el libro de visitas</h2>

                <form method="POST" action="/guestbook">

                    <fieldset>
                        <label for="name">Nombre</label>
                        <input type="text" name="name" id="name" />
                    </fieldset>

                    <fieldset>
                        <label for="message">Comentario</label>
                        <textarea name="message"></textarea>
                    </fieldset>

                    <button>Enviar</button>

                </form>
            `,
        })
    );
}

/**
 * ################
 * ## newMessage ##
 * ################
 */

function newMessage(request, response) {
    const body = request.body;

    // Si no hay "name" o "message" lanzamos un error.
    if (!body.name || !body.message) {
        response.statusCode = 400;
        response.setHeader('Content-type', 'text/html');
        response.end('Faltan campos en el formulario');
    } else {
        guestBook.unshift({
            name: body.name,
            message: body.message,
        });

        response.statusCode = 302;
        response.setHeader('Location', '/');
        response.end();
    }
}

/**
 * ##############
 * ## Noticias ##
 * ##############
 */

function news(request, response) {
    response.statusCode = 200;

    response.setHeader('Content-type', 'text/html');

    response.end(
        html({
            title: 'Noticias del día',
            content: '<p>Hoy pasaron muchas cosas</p>',
        })
    );
}

module.exports = { news, home, newMessage };
