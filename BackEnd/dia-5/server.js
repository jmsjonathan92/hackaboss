require('dotenv').config();
const http = require('http');
const querystring = require('querystring');
const path = require('path');
const fs = require('fs').promises;
const mime = require('mime-types');

const { news, home, newMessage } = require('./controllers');

const { HOST, PORT } = process.env;

const baseURL = `http://${HOST}:${PORT}`; // "localhost:3000"

const staticPath = path.join(__dirname, 'static');

const server = http.createServer();

// Función que analiza el body y lo traduce. Necesitamos crear una nueva promesa
// dado que esta función no da una respuesa inmediata. Lo hará cuando finalice
// el body.
const bodyParser = (request) => {
    return new Promise((resolve) => {
        let body = [];
        request.on('data', (buffer) => {
            body.push(buffer);
        });
        request.on('end', () => {
            body = Buffer.concat(body).toString();
            resolve(querystring.parse(body));
        });
    });
};

// Función que se ejecuta cada vez que el servidor recibe una nueva petición.
server.on('request', async (request, response) => {
    // Creamos un objeto URL.
    const urlInfo = new URL(request.url, baseURL);

    // Obtenemos el path de la URL.
    const urlPath = urlInfo.pathname;

    // Obtenemos los query strings y los guardamos en la request.
    request.querystring = urlInfo.searchParams;

    // Traducimos el body y lo guardamos nuevamente en la request.
    request.body = await bodyParser(request);

    const method = request.method;

    // Definimos el endpoint a "/noticias".
    if (urlPath === '/' && method === 'GET') {
        home(request, response);
    } else if (urlPath === '/noticias' && method === 'GET') {
        news(request, response);
    } else if (urlPath === '/guestbook' && method === 'POST') {
        newMessage(request, response);
    }

    // Si la ruta no es ninguna de las anteriores vamos a decirle al
    // servidor que examine las carpetas en busca de algún archivo
    // que coincida con la ruta definida.
    else {
        try {
            const resourcePath = path.join(staticPath, urlPath);

            const data = await fs.readFile(resourcePath);

            const mimeType = mime.lookup(resourcePath);

            response.statusCode = 200;
            response.setHeader('Content-type', mimeType);
            response.end(data);
        } catch (error) {
            response.statusCode = 404;
            response.setHeader('Content-type', 'text/html');
            response.end(`
                <p>Not found</p>
                <p><a href="/">Volver a la portada</a></p>
            `);
        }
    }

    response.end();
});

server.listen(PORT, () => {
    console.log(`Servidor funcionando en ${baseURL}`);
});
