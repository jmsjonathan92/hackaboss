/**
 * ################
 * ## Exercise 1 ##
 * ################
 *
 * Hacer un script de node que al ejecutarlo responda con el número de minutos
 * que pasaron desde la última vez que se ejecutó o con un mensaje que indique que
 * es la primera vez que se ejecuta.
 *
 */

const fs = require('fs').promises;
const { formatDistanceToNowStrict } = require('date-fns');
const { es } = require('date-fns/locale');

const start = async (filePath) => {
    try {
        // Intentamos leer el archivo que guarda la información de última ejecución.
        try {
            await fs.access(filePath);
        } catch (e) {
            throw new Error('Es la primera vez que me ejecutan');
        }

        // Leer el archivo de memoria
        let fileStats;

        // Intentamos acceder a las propiedades del archivo.
        try {
            fileStats = await fs.stat(filePath);
        } catch (error) {
            throw new Error(
                'Hubo un problema al acceder a las propiedades del fichero'
            );
        }

        // Calcular los minutos que han pasado desde la última ejecución.
        const distance = formatDistanceToNowStrict(fileStats.mtime, {
            locale: es,
            unit: 'minute',
        });

        // Mostramos la información por consola
        console.log(`La última vez que me ejecutaron fue hace ${distance}`);
    } catch (error) {
        // Mostrar que es la primera vez que se ejecuta.
        console.error(error.message);
    } finally {
        // Obtenemos la fecha actual.
        const now = new Date();

        // Creamos un objeto con dicha información.
        const lastExecutionInfo = {
            lastExecution: now.toISOString(),
        };

        // Guardamos el objeto transformado a cadena de texto en el archivo "filePath"
        await fs.writeFile(filePath, JSON.stringify(lastExecutionInfo));
    }
};

start('script.txt');
