/**
 * ################
 * ## Exercise 2 ##
 * ################
 *
 * Crea el módulo "readdir.js" de tal forma que:
 *
 *      - Lea recursivamente el contenido de subdirectorios.
 *
 *      - Permita que el directorio inicial se pase como argumento del programa
 *        p.ej: node readdir.js ../uploads
 *
 * Esto debería leer el directorio "uploads" en el directorio anterior y mostrar
 * el listado de contenidos indicando en el caso de que sea un fichero su tamaño
 * y fecha de creación.
 *
 * Esto debe hacerlo recursivamente para todos los posibles subdirectorios del
 * directorio inicial.
 *
 */

const fs = require('fs').promises;
const path = require('path');

const myPath = process.argv[2];

const checkFiles = async (dirPath) => {
    try {
        // Leemos todos los directorios y ficheros de la ruta que pasamos.
        const allData = await fs.readdir(dirPath);

        // Recorremos el array con los nombres de directorios y ficheros.
        for (const data of allData) {
            // Generamos la ruta al fichero o directorio actual.
            const dataPath = path.join(dirPath, data);

            // Obtener la info del fichero o directorio.
            const dataInfo = await fs.stat(dataPath);

            // Comprobamos si "data" es un directorio.
            if (dataInfo.isDirectory()) {
                console.log('==============');
                console.log(`Directorio ${data}`);
                // Si "data" es un directorio leemos de forma recursiva su contenido llamando
                // nuevamente a la propia función y pasándole la ruta del nuevo directorio.
                checkFiles(dataPath);
            } else {
                // Obtenemos la fecha de creación del archivo.
                const createdAt = new Date(
                    dataInfo.birthtime
                ).toLocaleDateString('es');

                console.log(
                    `Fichero ${data} [${dataInfo.size} bytes] ${createdAt}`
                );
            }
        }
    } catch (error) {
        console.log(error);
    }
};

checkFiles(myPath);
