/**
 * ################
 * ## Exercise 3 ##
 * ################
 *
 * Crear un script de node al que se le pase el nombre de un directorio por argumento,
 * p.ej: node create.js uploads
 *
 * El script debería crear el directorio si no existe.
 *
 */
