const path = require('path');

console.log(
    'Crear una ruta de forma segura:',
    path.join(__dirname, '../dia-2')
);

console.log('Corrección de ruta:', path.normalize('..///dia-2'));

console.log('Extensión del fichero:', path.extname('./path-module.js'));
