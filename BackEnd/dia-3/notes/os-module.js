const os = require('os');

console.log('Memoria total:', os.totalmem());
console.log('Memoria libre:', os.freemem());
console.log('Hostname:', os.hostname());
console.log('Ruta usuario:', os.homedir());
console.log('Ruta archivos temporales:', os.tmpdir());
