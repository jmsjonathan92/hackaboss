const fs = require('fs');

const myCallback = (err) => {
    if (err) {
        console.error(err);
    } else {
        console.log('El fichero ha sido creado');
    }
};

fs.writeFile('./data/file.txt', 'Hello dog!', myCallback);

try {
    fs.writeFileSync('./data/file2.txt', 'Hello againg');
    console.log('El fichero ha sido creado');
} catch (error) {
    console.error(error.message);
}
