const fs = require('fs/promises');
const { provincias } = require('./data/provincias.json');

const readFile = async (filename) => {
    try {
        const data = await fs.stat(filename);
        console.log(data);
    } catch (error) {
        throw new Error(error.message);
    }
};

readFile('./data/file2.txt');

const writeFile = async (route, content) => {
    try {
        await fs.writeFile(route, content);
    } catch (error) {
        if (error.code === 'ENOENT') {
            console.error('No se encuentra el directorio');
        } else {
            console.error(error.message);
        }
    }
};

// writeFile('./data/file3.txt', 'Viva node_modules!!');

const removeFile = async (route) => {
    try {
        await fs.unlink(route);
    } catch (error) {
        console.error(error.message);
    }
};

// removeFile('./data/file3.txt');

const getMetadata = async (route) => {
    try {
        const data = await fs.stat(route, 'utf-8');
        console.log(data);
    } catch (error) {
        console.log(error.message);
    }
};

// getMetadata('./data/file2.txt');

// console.log(provincias);

const persons = [
    {
        name: 'Laura',
        age: 32,
    },
    {
        name: 'Paco',
        age: 25,
    },
];

// writeFile('./data/persons.json', JSON.stringify(persons));
