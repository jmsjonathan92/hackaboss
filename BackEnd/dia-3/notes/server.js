// El fichero ".env" debe estar ubicado obligatoriamente en la raíz del proyecto,
// en este caso en "dia-3". De lo contrario no funcionará.
require('dotenv').config();

const { MYSQL_HOST, MYSQL_PASSWORD } = process.env;

console.log(MYSQL_HOST);
console.log(MYSQL_PASSWORD);
