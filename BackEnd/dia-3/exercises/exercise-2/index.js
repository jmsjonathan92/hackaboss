/**
 * ################
 * ## Exercise 2 ##
 * ################
 *
 * Usando el módulo fs de NodeJS, escribe una función que acepte como argumentos
 * dos strings. El primer argumento representará un nombre de archivo y el segundo
 * un contenido a escribir en el archivo.
 *
 * Llama a esta función para escribir un fichero file.txt en el directorio actual
 * el contenido "Hola Backend!".
 *
 * Pista: Puedes usar callbacks o promises, aunque lo recomendable es lo segundo.
 *
 */

const fs = require('fs').promises;

const [myRoute, myContent] = process.argv.slice(2);

const writeFile = async (route, content) => {
    try {
        await fs.writeFile(route, content);
        console.log('El fichero se ha creado correctamente');
    } catch (error) {
        console.log(error.message);
    }
};

writeFile(myRoute, myContent);
