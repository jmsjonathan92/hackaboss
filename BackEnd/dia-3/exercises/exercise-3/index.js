/**
 * ################
 * ## Exercise 3 ##
 * ################
 *
 * Usando el módulo fs de NodeJS, escribe una función que acepte como argumento
 * un string:
 *
 *      - La función deberá imprimir el string "Es un directorio" si el Path que
 *        le pasamos es un directorio.
 *
 *      - La función deberá imprimir el string "Es un archivo" si el Path que le pasamos
 *        es un archivo.
 *
 *      - La función deberá imprimir el string "No existe" si el Path que le pasamos no
 *        existe.
 *
 * Pista: Puedes usar callbacks o promises, aunque lo recomendable es lo segundo.
 *
 */

const fs = require('fs').promises;

const readFile = async (route) => {
    try {
        await fs.readFile(route);
        console.log('Es un archivo');
    } catch (error) {
        if (error.code === 'EISDIR') {
            console.log('Es un directorio');
        } else if (error.code === 'ENOENT') {
            console.log('El archivo o directorio no existe');
        } else {
            console.error(error.message);
        }
    }
};

const whatIsThis = async (route) => {
    try {
        const data = await fs.stat(route).isDirectory();

        const result = await data.isDirectory();

        if (result) {
            console.log('Es un directorio');
        } else {
            console.log('Es un archivo.');
        }
    } catch (error) {
        if (error.code === 'ENOENT') {
            console.log('El archivo o directorio no existe');
        } else {
            console.error(error.message);
        }
    }
};

whatIsThis('./index.js');
