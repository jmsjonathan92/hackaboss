const http = require('http');
const querystring = require('querystring');

// Creamos un servidor HTTP.
const server = http.createServer();

// Definimos el puerto.
const PORT = 3000;

// Función que traduce el body.
const bodyParser = (req) => {
    return new Promise((resolve) => {
        let body = [];

        req.on('data', (buffer) => {
            body.push(buffer);
        });

        req.on('end', () => {
            body = Buffer.concat(body).toString();
            resolve(querystring.parse(body));
        });
    });
};

// Configuramos una función que se ejecutará cuando al servidor le llegue
// una petición cualquiera.
server.on('request', async (req, res) => {
    // Llamamos a la función "bodyParser" y le pasamos la request.
    const body = await bodyParser(req);

    console.log(body);

    res.statusCode = 200;
    res.setHeader('Content-type', 'text/html');
    res.end('<h1>Hello World</h1>');
});

// Ponemos el servidor a escuchar.
server.listen(PORT, () => {
    console.log(`El servidor está funcionando en http://localhost:${PORT}`);
});
