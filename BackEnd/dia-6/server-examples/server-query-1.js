const http = require('http');

// Creamos un servidor HTTP.
const server = http.createServer();

// Definimos el puerto.
const PORT = 3000;

// Configuramos una función que se ejecutará cuando al servidor le llegue
// una petición cualquiera.
server.on('request', (req, res) => {
    // Para leer los queryparams necesitamos crear el objeto URL.
    const urlInfo = new URL(req.url, `http://localhost:${PORT}`);

    // Objeto vacío donde iremos metiendo los parámetros de la query.
    const obj = {};

    // Recorremos cada uno de los parámetros y los agregamos como propiedades del
    // objeto anterior. Cada parámetro vendrá en forma de array con dos posiciones
    // "[propiedad, valor]".
    for (const param of urlInfo.searchParams) {
        const [key, value] = param;
        obj[key] = value;
    }

    console.log(obj);

    res.statusCode = 200;
    res.setHeader('Content-type', 'text/html');
    res.end('<h1>Hello World</h1>');
});

// Ponemos el servidor a escuchar.
server.listen(PORT, () => {
    console.log(`El servidor está funcionando en http://localhost:${PORT}`);
});
