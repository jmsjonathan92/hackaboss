const http = require('http');
const querystring = require('querystring');

const server = http.createServer();

const PORT = 3000;

server.on('request', (req, res) => {
    // Hacemos un split en el caracter interrogación para dividir los queryparams
    // del resto de la ruta. Nos quedamos con el index 1 del array que se genera
    // (el querystring).
    const myQueryString = req.url.split('?')[1];

    // Transformamos el querystring en un objeto JavaScript haciendo uso de core
    // module "querystring".
    const parsedParams = querystring.parse(myQueryString);

    // Obtenemos el valor de la propiedad "type".
    const { type } = parsedParams;

    // Como todas las respuestas van a recibir el mismo "statusCode" y van a enviar
    // el mismo tipo de dato podemos definir esa información aquí mismo.
    res.statusCode = 200;
    res.setHeader('Content-type', 'text/html');

    // En función del valor de "type" enviamos una respuesta u otra.
    if (type === 'human') {
        res.end('<h1>Has filtrado a los humanos</h1>');
    } else if (type === 'alien') {
        res.end('<h1>Has filtrado a los aliens</h1>');
    } else {
        res.end('<h1>Obtienes a todos los humanos y aliens</h1>');
    }
});

server.listen(PORT, () => {
    console.log(`El servidor está funcionando en http://localhost:${PORT}`);
});
