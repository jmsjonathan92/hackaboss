const http = require('http');

// Creamos un servidor HTTP.
const server = http.createServer();

// Definimos el puerto.
const PORT = 3000;

// Configuramos una función que se ejecutará cuando al servidor le llegue
// una petición cualquiera.
server.on('request', (req, res) => {
    res.statusCode = 200;
    res.setHeader('Content-type', 'text/html');
    res.end('<h1>Hello World</h1>');
});

// Ponemos el servidor a escuchar.
server.listen(PORT, () => {
    console.log(`El servidor está funcionando en http://localhost:${PORT}`);
});
