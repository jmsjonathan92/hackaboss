const http = require('http');

const server = http.createServer();

const obj = {
    curso: 'backend',
};

server.on('request', (req, res) => {
    res.statusCode = 200;
    res.setHeader('Content-type', 'application/json');
    res.end(JSON.stringify(obj));
});

server.listen(3000, () => {
    console.log('Servidor funcionando en http://localhost:3000');
});
