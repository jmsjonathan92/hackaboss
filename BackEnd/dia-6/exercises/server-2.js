const http = require('http');

const server = http.createServer();

const obj = {
    curso: 'backend',
};

const world = {
    message: 'Hello world!',
};

server.on('request', (req, res) => {
    res.statusCode = 200;
    res.setHeader('Content-type', 'application/json');

    if (req.url === '/curso') {
        res.end(JSON.stringify(obj));
    } else {
        res.end(JSON.stringify(world));
    }
});

server.listen(3000, () => {
    console.log('Servidor funcionando en http://localhost:3000');
});
