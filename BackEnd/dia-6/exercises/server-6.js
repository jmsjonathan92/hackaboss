const http = require('http');

const server = http.createServer();

const bodyParser = (request) => {
    return new Promise((resolve) => {
        let body = [];
        request.on('data', (buffer) => {
            body.push(buffer);
        });
        request.on('end', () => {
            body = Buffer.concat(body).toString();
            resolve(JSON.parse(body));
        });
    });
};

server.on('request', async (req, res) => {
    const { url, method } = req;

    // Leemos el fichero "database.json" y lo transformamos de forma automática
    // en un objeto JS.
    const database = require('./database/database.json');

    if (url === '/api/messages' && method === 'GET') {
        res.statusCode = 200;
        res.setHeader('Content-type', 'application/json');
        res.end(JSON.stringify(database));
    } else if (url === '/api/messages' && method === 'POST') {
        const body = await bodyParser(req);
        database.push(body);

        res.statusCode = 200;
        res.setHeader('Content-type', 'application/json');
        res.end(JSON.stringify(database));
    } else {
        res.statusCode = 404;
        res.end();
    }
});

server.listen(3000, () => {
    console.log('Servidor funcionando en http://localhost:3000');
});
