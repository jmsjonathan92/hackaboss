const http = require('http');

const server = http.createServer();

const obj = {
    curso: 'backend',
};

const world = {
    message: 'Hello world!',
};

const notFound = {
    message: 'No lo encuentro',
};

server.on('request', (req, res) => {
    res.setHeader('Content-type', 'application/json');

    if (req.url === '/curso') {
        res.statusCode = 200;
        res.end(
            JSON.stringify({
                ...obj,
                method: req.method,
                url: req.url,
            })
        );
    } else if (req.url === '/message') {
        res.statusCode = 200;
        res.end(
            JSON.stringify({
                ...world,
                method: req.method,
                url: req.url,
            })
        );
    } else {
        res.statusCode = 404;
        res.end(
            JSON.stringify({
                ...notFound,
                method: req.method,
                url: req.url,
            })
        );
    }
});

server.listen(3000, () => {
    console.log('Servidor funcionando en http://localhost:3000');
});
