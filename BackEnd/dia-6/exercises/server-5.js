const http = require('http');

const server = http.createServer();

const bodyParser = (request) => {
    return new Promise((resolve) => {
        let body = [];
        request.on('data', (buffer) => {
            body.push(buffer);
        });
        request.on('end', () => {
            body = Buffer.concat(body).toString();
            resolve(JSON.parse(body));
        });
    });
};

server.on('request', async (req, res) => {
    const { url, method } = req;

    if (url === '/data' && method === 'POST') {
        const body = await bodyParser(req);
        res.statusCode = 200;
        res.setHeader('Content-type', 'application/json');
        res.end(JSON.stringify(body));
    } else {
        res.statusCode = 404;
        res.end();
    }
});

server.listen(3000, () => {
    console.log('Servidor funcionando en http://localhost:3000');
});
