const express = require('express');

const app = express();

app.use((req, res) =>
    res.send({
        name: 'David',
        age: 12,
    })
);

app.listen(3000, () => {
    console.log('Server ON!');
});
