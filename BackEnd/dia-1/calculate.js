const { sum, sub } = require('./lib/helpers');

const result_A = sum(2, 4);

console.log('Resultado de la suma: ' + result_A);

const result_B = sub(10, 5);

console.log('Resultado de la resta: ' + result_B);
