const flag = true;

let imported;

if (flag) {
    imported = require('./lib/hello');
} else {
    imported = require('./lib/welcome');
}

imported.welcome();
