/**
 * ############
 * ## Buffer ##
 * ############
 *
 * Un búfer es un espacio en la memoria (en general, RAM) que almacena datos binarios. Podemos acceder a estos espacios de memoria
 * hacuendo uso de la clase Buffer. Los búferes almacenan datos de forma similar a un array. La principal diferencia es que una vez
 * creado el buffer este no puede ser modificado.
 *
 * Cuando se realizan solicitudes HTTP, estas devuelven flujos de datos que se almacenan temporalmente en un búfer interno. Por ejemplo,
 * cuando el servidor recibe una petición de tipo post, esta incluye un body en formato buffer. Si queremos descifrar la información
 * de dicho body debemos convertir el buffer a un formato más adecuado.
 *
 * ¡Apunte! Serializar consiste en el proceso de convertir un objeto en memoria en una cadena de bytes para poder guardarlo en el disco o
 * enviarlo a través de la red. Deserializar es el proceso inverso: convertir una cadena de bytes en un objeto en memoria.
 *
 */

const querystring = require('querystring');

// Imaginemos que un nuevo usuario se registra en nuestra web. Lo que haremos en el
// frontend será enviar una petición al backend. En esta petición incluiremos el body.
const body = {
    username: 'david97',
    email: 'david@email.com',
    password: '123456',
};

// El body es convertido de forma automática a texto antes de ser convertido en un
// buffer (también sería posible convertirlo a texto JSON).
const stringifyBody = querystring.stringify(body);

console.log('\nObjeto convertido a cadena de texto:', stringifyBody);

// Para facilitar el viaje de este body a través de la red se convierte en un buffer de
// datos. Este proceso se llama serialización.
const serializedBody = Buffer.from(stringifyBody);

console.log('\nBuffer serializado:', serializedBody);

// Deserializamos el buffer y lo transformamos en una cadena de texto.
const deserializedBody = Buffer.concat([serializedBody]).toString();

console.log('\nBuffer deserializado:', deserializedBody);

// Transformamos el texto anterior en un objeto nuevamente.
const parsedBody = querystring.parse(deserializedBody);

console.log('\nBody recuperado: ', parsedBody);
