const sharp = require('sharp');
const path = require('path');

const editPhoto = async () => {
    try {
        const imagePath = path.join(__dirname, 'pics', 'lula.jpg');
        const imageOutputPath = path.join(__dirname, 'edited', 'lula.png');

        const image = sharp(imagePath);

        image.resize(400, 400, {
            fit: 'outside',
        });

        image.normalize(true).toFormat('png').rotate(90).flip(true);

        await image.toFile(imageOutputPath);

        console.log('La imagen ha sido modificada');
    } catch (error) {
        console.log(error);
    }
};

editPhoto();
