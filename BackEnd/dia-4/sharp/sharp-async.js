const sharp = require('sharp');
const path = require('path');

const editPhoto = async () => {
    try {
        // Generamos las rutas: la ruta de la imagen que vamos a utilizar y
        // la ruta donde vamos a guardar la imagen modificada.
        const imagePath = path.join(__dirname, 'pics', 'pikachu.gif');
        const imageOutputPath = path.join(__dirname, './edited/pikachu-2.jpg');

        // Accedemos a la imagen.
        const image = sharp(imagePath);

        // Modificamos la imagen.
        image.tint('rgb(255, 200, 0)');

        // Guardamos la imagen.
        await image.toFile(imageOutputPath);

        console.log('La imagen ha sido modificada');
    } catch (error) {
        console.log(error);
    }
};

editPhoto();
