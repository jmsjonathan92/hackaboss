const sharp = require('sharp');

// Indicamos la ruta con texto (no recomendado).
const image = sharp('./pics/pikachu.gif');

// Modificamos la imagen.
image.grayscale();
image.blur(3);
image.resize(400, 400);

// Guardamos la imagen.
image
    .toFile('./edited/pikachu.jpg')
    .then(() => console.log('Imagen modificada'))
    .catch((error) => console.log(error));
