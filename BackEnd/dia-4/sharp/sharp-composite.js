const sharp = require('sharp');
const path = require('path');

const editPhoto = async () => {
    try {
        const logoPath = path.join(__dirname, 'pics', 'pokeball.png');
        const imagePath = path.join(__dirname, 'pics', 'pikachu.gif');
        const imageOutputPath = path.join(__dirname, 'edited', 'pikachu-3.png');

        const image = sharp(imagePath);

        image.composite([
            {
                input: logoPath,
                gravity: 'south',
            },
        ]);

        await image.toFile(imageOutputPath);

        console.log('La imagen ha sido modificada');
    } catch (error) {
        console.log(error);
    }
};

editPhoto();
