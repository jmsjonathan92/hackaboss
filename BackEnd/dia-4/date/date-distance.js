const {
    formatDistance,
    add,
    sub,
    differenceInCalendarDays,
    isFriday,
} = require('date-fns');
const { es } = require('date-fns/locale');

const date1 = new Date(2021, 4, 25, 19, 30);
const date2 = new Date();

// Distancia entre dos fechas en formato humano.
const distance = formatDistance(date1, date2, {
    locale: es,
});

console.log(distance);

// Restar a una fecha unos dias, meses o años determinados (también se puede
// sumar con "add").
const pastDate = sub(date2, {
    months: 1,
    days: 4,
});

console.log('Fecha actual:', date2.toLocaleDateString('es'));

console.log('Fecha pasada:', pastDate.toLocaleDateString('es'));

// Restamos la diferencia en días de una fecha a otra.
const difference = differenceInCalendarDays(date2, pastDate);

console.log(difference);

// Comprobamos si el día es viernes.
console.log(isFriday(pastDate));
