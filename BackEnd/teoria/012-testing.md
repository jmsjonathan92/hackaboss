# Testing End2End

Una parte fundamental del desarrollo software es el testing automatizado. En los ejemplos anteriores habeis porbado vuestros desarrollos manualmente mediante el uso de Postman (o httpie) para realizar peticiones a vuestro servicio y comprobar que la respuesta del servicio es la que debe ser. 

Existen herramientas para automatizar estos procesos de prueba, son las denominadas herramientas de testing automatizado. 

Veremos en esta sección el uso de [jest](https://jestjs.io/es-ES/) y [supertest](https://github.com/visionmedia/supertest) para de manera muy sencilla definir y ejecutar tests automaticos sobre nuestros servicios. 

## Instalación y uso básico de Jest

Para instalar jest ejecutamos el siguiente comando dentro de la carpeta raíz de nuestro proyecto

```bash
npm install --save-dev jest
```

A continuación añadimos dos scripts a la lista de scripts del fichero package.json

```json
"scripts": {
  "test": "jest",
  "test:watch": "jest --watch"
},
```

# Probando Jest 

En un nuevo fichero definiremos un test con la siguiente sintaxis

```js
it('Testing to see if Jest works', () => {
  expect(1).toBe(1)
})
```

y ejeucutamos el comando 

```bash
npm run test
```

Y podemos ver en el terminal la salida de los tests que nos indicará si nuestras pruebas al resultado ok o no 

Con el comando 

```bash
npm run test:watch
```

Jest relanzará automáticamente los tests cuando modifquemos el código automáticamente.

## Tests asíncronos

Cuando en nuestros tests probemos alguna funcion asíncrono debemos definir la función de prueba como asíncrona

```js
it('Async test', async done => {
  // Do your async tests here
  
  done()
})
```

# Testeando servicios web express con Supertest

Para testear nustros endpoints podemos usar supertest que nos permite lanzar peticiones al servicio y evaluar si la respuesta obtenida es satisfactoria de una forma muy cómoda.

Instalamos supertest

```bash 
npm install supertest --save-dev
```

Para poder usar supertest necesitamos separar la definición de la app express de la llamada al método listen. Hasta ahora hemos hecho algo como esto en nuestros index.js

```js
const express = require('express')
const app = express()

// Middlewares...
// Routes...

app.listen(3000)
```

Pero ahora vamos a separar esto en dos ficheros:

```js
//server.js
const express = require('express')
const app = express()

// Middlewares...
// Routes...

app.get('/api/users', async (req, res) => {
  res.json({message: 'Hello World!'})
})

module.exports = app
```

Esto es un modulo que exporta la app

```js
// start.js
const app = require('./server.js')
app.listen(3000)
```

Y este script importa la app definida en server.js y la "levanta", es decir, llama al metodo listen apra que el servicio express arranque y escuche peticiones en el puerto indicado


## Usando supertest

Para usar supertest para realizar peticiones a nuestro servicio haremos algo como

```js
// server.test.js
// Importamos nuestra app express
const app = require('./server') 
// importamos supertest
const supertest = require('supertest')
// inicializamos supertest con nuestra app
const request = supertest(app)


// Definimos un test asincrono ya que las peticiones se relaizan mediante llamadas asincronas
it('Gets the test endpoint', async done => {
  // Sends GET Request to /test endpoint
  const res = await request.get('/api/users')

  expect(response.status).toBe(200)
  expect(response.body.message).toBe('pass!')
  done()
})

```

Ahora si ejecutamos 

```bash
npm run test:watch
```

Veremos nuestros test en acción

## Ejercicios

Escribamos tests para el `api/users` de los ejercios del capitulo 8



