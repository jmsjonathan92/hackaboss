# Git

**Git** es  un sistema  de control de versiones. Nos permite llevar una cronología de los cambios que se han efectuado sobre un conjunto de ficheros, navegar por estos cambios en el tiempo, y sincronizar el estado de dichos archivos con un servidor externo.

Al conjunto de los archivos que **git** maneja dentro de un determinado directorio se le conoce como **repositorio**. Al contrario de lo que ocurría con otros sistemas de control de versiones anteriores, **git** nos permite tener una copia totalmente funcional del repositorio en nuestra máquina de trabajo, sin necesidad de depender de un servidor externo. Sin embargo, la verdadera potencia de este sistema está en el hecho de poder sincronizar nuestro repositorio local con la copia del repositorio que está alojada en un servidor externo especializado (como pueden ser `github`, `gitlab` o `bitbucket`) y poder así trabajar en equipo con otras personas que también hacen cambios sobre los archivos del repositorio.

Para inicializar  un repositorio git usaremos el comando:
```
git init
```

Esto creará un directorio oculto, que se llamará `.git`, y que **git** usará para llevar el cálculo  de los cambios que han sufrido los archivos observados.


Cuando hacemos modificaciones sobre el código, podemos añadir estos cambios al registro (**stage**) de **git**:
* Para añadir un fichero o directorio concreto al registro de cambios: `git add <fichero/directorio>`
* Para añadir el directorio actual al registro de cambios: `git add .`

Cuando estamos satisfechos con los cambios que hemos hecho, y después de haberlos añadido al regitro de cambios que **git** está manejando en ese momento (o lo que es lo mismo, los hemos puesto en **stage**), podemos hacer un **commit**, es decir, una confirmación de que los cambios que hemos introducido son los correctos y pueden ser sincronizados. Para confirmar los cambios actuales (hacer un *commit*):
```
git commit -m "Mensaje descriptivo"
```

Si creamos un repositorio remoto para sincronización, tendremos que seguir las instrucciones del sitio para indicarle a **git** cómo debe sincronizarse. Para comprobar si tenemos configurado un repositorio remoto podemos ejecutar el comando
```
git remote -v
```

En caso de tener configurado un repositorio remoto podemos sincronizar los commits que tengamos en nuestro repositorio local usando el comando
```
git push origin master
```

En caso de que lo estemos haciendo por primera vez usaremos
```
git push -u origin master
```

Una de las características más útiles de **git** es poder crear diferentes ramas de trabajo. Cada rama (*branch*) representa un estado del repositorio. Por convención, a la rama principal se le denomina `master`, (aunque en algunos sitios se ha empezado a recomendar el nombre `main`) y por lo general los cambios que queramos llevar a producción, es decir, el estado final de nuestro código, estará reflejado siempre en esa rama.

Si queremos hacer una rama de trabajo nueva en nuestro directorio (por ejemplo, para probar un cambio en nuestro código que no estamos seguros si va a funcionar) tenemos que ejecutar:
```
git checkout -b <nombre>
```
Esto además de crear una rama nueva, situará el *puntero* de **git** sobre ella, con lo que los cambios que hagamos ahora a los archivos, modificarán el estado de los archivos únicamente en esa rama, y no afectarán a otras ramas. Para volver a la rama master (después de confirmar o descartar los cambios) ejecutamos:
```
git checkout master
```

Y si queremos volver a la rama nueva que hemos creado anteriormente
```
git checkout <nombre>
```

Para sincronizar esa rama con el repositorio remoto (si tenemos) usamos
```
git push origin <nombre>
```

Si queremos borrar una rama ejecutamos
```
git branch -d <nombre>
```

Pero si los cambios que realizamos en esa rama finalmente nos convencen y queremos guardarlos en la rama principal tenemos que hacer un **merge**. El merge siempre se hace desde la rama que va a recibir los cambios por ejemplo, si vamos a la rama `master` y hacemos
```
git merge <nombre>
```

Esto hará que los cambios **confirmados** que teníamos en la rama <nombre> se *mezclen* con estado actual de la rama `master`. Para hacer un **merge** no puede haber cambios pendientes de confirmación (*commit*) en ninguna de las dos ramas.

Para ver el registro de los **commits** realizados podemos usar el comando
```
git log
```

La mayoría de IDEs y algunos editores de texto ofrecen herramientas gráficas para la gestión de un repositorio **git**. Por ejemplo, en el caso de usar **Visual Studio Code**, tendremos disponible una zona específica para el sistema de control de versiones, en la que podremos añadir archivos, realizar **commits**, **rollbacks**, **push**, comparar versiones, etc. con unos pocos clicks.

### gitignore
Por lo general en todos los repositorios suele existir un archivo especial, `.gitignore`, que define los nombres de archivos o las extensiones que no queremos compartir en el repositorio. Por ejemplo, podemos tener un archivo de texto con nuestros passwords, o alguna configuración específica de nuestra máquina que no tendría sentido compartir con el resto de nuestro equipo.