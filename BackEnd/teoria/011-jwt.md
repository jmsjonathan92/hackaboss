# Autenticación usando JWT
**JWT** significa *JSON Web Token*. Un *JWT* no es más que un *string*, que tiene tres partes codificadas en *Base64* separadas por un punto. Más adelante veremos qué significa y para qué usamos cada una de estas partes. Aunque es solamente un **token** (un *string*) y en él podemos guardar cualquier tipo de *data* o *metadata*, el uso más común para estos *tokens* es ayudanos a gestionar la autenticación y el acceso de los diferentes usuarios a nuestro sistema.

### Cookies vs JWT
Durante mucho tiempo se han venido usando **cookies** para autenticar, autorizar y gestionar sesiones de usuarios en servidores. Desde hace unos años, con la popularización de los **JWT**, su uso ha decrecido en favor de estos últimos. Las *cookies* como mecanismo de gestión de sesiones son *stateful* (con estado), esto es, en el servidor y en el cliente se guarda el mismo pedazo de información sobre el usuario (por lo general un identificador de sesión), mientras que los *JWT* suelen usarse como mecanismo de autenticación *stateless* (sin estado), es decir, el servidor no guarda información del *JWT* una vez lo emite, sino que confía en la información del *JWT* recibido desde el cliente.

Obviamente esta confianza del servidor está basada en la criptografía. Cada *JWT* está firmado con una clave criptográfica que el servidor conoce, y así puede verificar que el contenido del *JWT* no ha sido alterado por el cliente.

Aquí vemos un ejemplo típico de comunicación entre cliente y servidor usando *cookies*, y otro usando *JWT*. Como vemos, en ambos modos el cliente manda unas credenciales, y se le responde con una cookie o un JWT, según el caso. En las siguientes llamadas el cliente deberá enviar esa cookie o JWT, pero la diferencia radica en que el servidor en el segundo caso no tiene constancia de ninguna sesión de usuario, simplemente verifica que el JWT es correcto en cada llamada.

![JWT vs cookies](https://1.bp.blogspot.com/-5C7E7LEwX2A/XBPFDI7Y0jI/AAAAAAAAH7o/rfWkNyOw49IIUK4x9FS4329jHnkJ5QXbACLcBGAs/s1600/Cookie-v-Token-Diagram-v1-3-1024x536.png)

Usar *JWT* tiene varias ventajas. Como hemos visto no es necesario que el sevidor guarde una sesión para el usuario en memoria/base de datos, por lo cual es más sencillo enviar *request* a un clúster de servidores (no necesitan tener acceso común a una base de datos de sesiones). También favorece el poder tener por ejemplo un servidor que se encarge de gestionar sólo la autenticación de usuarios y expedir los *JWT*, para después usar ese *JWT* en otro servidor del mismo sistema que se encarge de otra tarea, por ejemplo gestionar los pedidos de una tienda online, sin tener que comunicar el sistema de autenticación con el de pedidos, ya que este segundo puede verificar el *JWT* simplemente teniendo la firma criptográfica.

---

### Partes de un JWT
Como hemos dicho, un *JWT* no es más que un string que tiene tres partes: **header**, **payload** y **signature**.

* **Header**: encabezado dónde se indica, al menos, el algoritmo y el tipo de *token*.
* **Payload**: información contenida en el *token*. Por lo general datos de usuario y privilegios.
* **Signature**: firma del *token*. Permite la comunicación segura entre cliente y servidor.

En la web [jwt.io](jwt.io) existe un codificador/decodificador de *JWT* muy fácil de usar y que nos puede servir para explorar como se contruyen y deconstruyen *JWT*.

### *JWT* en NodeJS
Para trabajar con *JWT* en el contexto de NodeJS, usaremos la librería de jsonwebtoken de npm: `jsonwebtoken`.

```
npm i jsonwebtoken
```

Esta librería nos facilita dos funciones, que usaremos para **firmar** y **verificar** *JWT*.

```javascript
const jwt = require('jsonwebtoken');
const token = jwt.sign(payload, secret); // esto meterá el payload dentro del token
const payload = jwt.verify(token, secret); // esto leerá el payload del token 
```

Estas dos funciones son así de simples. `jwt.sign` creará un *token* con la información proporcionada en el *payload*, y firmará el *token* usando el *secret* (una clave secreta almacenada en nuestro servidor). Después, para verificar y leer los datos almacenados en ese *JWT*, podremos hacer uso de la función `jwt.verify`, que además de verificar que el *JWT* no ha sido alterado desde su creación usando el mismo *secret*, nos devolverá la información guardada en el payload del *JWT*.

---

## Gestión de cuentas de usuarios y accesos usando JWT
En los dos apartados anteriores hemos visto cómo se trabaja con *JWT* de forma genérica, pero *JWT* tiene un uso para el que destaca: el manejo de cuentas de usuarios y accesos a nuestro backend. Vamos entonces a ver cómo podemos aplicar *JWT* a ello.

Como hemos visto, cuando un usuario envía una *request* a nuestro backend, en algunos casos queremos que esa *request* sea autenticada, es decir, contenga un secreto que sólo conoce el usuario que envía la *request*, de forma que nuestro backend pueda permitirle el acceso a partes de nuestro backend para las que el usuario haya sido autorizado, o le pertenezcan. Por ejemplo en una red social, las peticiones de un usuario deberán ir con este secreto, de tal forma que el backend pueda identificar qué usuario está mandando la *request*, y darle acceso únicamente a las partes de la red social que el usuario tiene asignadas.

Este secreto que el usuario envía en cada *request*, suele transferirse usando el header `Authorization`. Cuando un usuario hace *login* (se identifica en nuestro backend) a través de un cliente *HTTP* (web, aplicación móvil, dispositivo IOT, etc.) se le suele devolver ese secreto como respuesta a la petición de login. Es responsabilidad del cliente guardar de manera segura ese secreto, pues es su tarjeta de identificación, que deberá usar en cada petición siguiente al backend.

En las llamadas siguientes al backend después de haber hecho *login*, el cliente enviará este header en la *request*, dentro de la clave `Authorization`. Sólo de esta forma el backend podrá identificar al cliente y darle acceso a lo que proceda.

### Creando usuarios en nuestro backend
Para crear un nuevo usuario, por lo general, necesitaremos los datos del usuario que proceda (email, etc.) y por lo general, una contraseña de acceso. Esta contraseña no es recomendable almacenarla en claro en una base de datos. Es decir, no almacenaremos la password tal cual, si no que usaremos una **función hash**. Una *función hash* no es más que un algoritmo determinista que transforma una cadena de caracteres en otra cadena de caracteres de longitud fija. Es decir, dado un *string* de logitud N, la función devolverá otro *string* con una logitud fija M, independientemente del valor N. Más información sobre las funciones de hashing: [Funciones Hash](https://es.wikipedia.org/wiki/Funci%C3%B3n_hash)

Vamos a verlo con un ejemplo. Para generar hashes en NodeJS vamos a usar la librería `bcryptjs`:

```
npm i bcryptjs
```

Si por ejemplo queremos crear un *hash* a partir de un *string*:

```javascript
const bcrypt = require('bcryptjs');

const passwordHash = bcrypt.hashSync('myPassword', 10); // passwordHash: '$2a$10$AMnGhQBViZI4JpN2zdjBX.DnxwbWdSPgkyPgUGxLbjt.oYO1vrWMi'
```

Ahora imaginemos que hemos guardado en nuestra base de datos ese *hash* en lugar de la password. La razón para hacer esto, es que si por ejemplo hubiese un incidente de seguridad y un atacante nos robase una copia de nuestra base de datos, no podría ver los passwords de nuestros usuarios, lo único que vería serían *hashes* de esos passwords. Dado que el *hash* no es revertible, el atacante nunca podría saber cuáles son los passwords originales de los usuarios y aunque tuviese una copia de la base de datos, no podría hacer login en nuestro backend suplantando a ninguno de nuestros usuarios.

Así pues, como deciamos, tenemos almacenados en nuestra base de datos los *hashes* de los passwords, no los passwords originales. Entonces, tenemos que tener un mecanismo para comprobar, cuando un usuario hace *login*, que realmente nos está mandando su password original correcto. La librería `bcryptjs`, además de generar *hashes*, también puede comprobar si un string dado (el password original) es el string que generó un determinado *hash*.

```javascript
const bcrypt = require('bcryptjs');

const passwordHash = bcrypt.hashSync('myPassword', 10); // passwordHash: '$2a$10$AMnGhQBViZI4JpN2zdjBX.DnxwbWdSPgkyPgUGxLbjt.oYO1vrWMi'
const isValid = bcrypt.compareSync('myPassword', passwordHash); // true
const isValid2 = bcrypt.compareSync('otroPasswordDiferente', passwordHash); // false
```

Así, cuando un usuario intente hacer *login*, va a mandarnos su password al backend. Este password lo deberemos comparar usando bcryptjs con el password que teníamos almacenado en base de datos.

Cabe decir también que aunque hemos usado en los ejemplos la variante síncrona de las funciones de `bcryptjs`, esta librería tiene también funciones en su versión asíncrona que devuelven **promesas** o pueden ser usadas mediante callbacks. Los ejemplos anteriores, usando funciones asíncronas, podrían valernos para escribir nuestras funciones asíncronas para creación de usuarios y login:

```javascript
const Joi = require('joi');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const { database } = require('../infrastructure');

const userSchema = Joi.object({
  name: Joi.string(),
  email: Joi.string().email().required(),
  password: Joi.string().min(5).max(20).required(),
});

async function createUser(req, res) {
  try {
    const { name, email, password } = req.body;
    await userSchema.validateAsync({ name, email, password });
  
    const passwordHash = await bcrypt.hash(password, 10);
    const query = 'INSERT INTO users (name, email, password) VALUES (?, ?, ?)';
    await database.pool.query(query, [name, email, passwordHash]);
  
    res.send('Usuario creado');
  } catch (err) {
    next(err);
  }
}

async function login(req, res, next) {
  try {
    const { email, password }= req.body;

    const schema = Joi.object({
      email: Joi.string().email().required(),
      password: Joi.string().min(5).max(20).required(),
    });

    await schema.validateAsync({ email, password });

    // 1. Recuperamos el usuario desde la base de datos.

    const query = 'SELECT * FROM users WHERE email = ?';
    const [rows] = await database.pool.query(query, email);

    if (!rows || !rows.length) {
      const error = new Error('No existe el usuario');
      error.code = 404;
      throw error;
    }

    const user = rows[0];

    // 2. Comprobamos que el password que nos están enviando es válido.

    const isValidPassword = await bcrypt.compare(password, user.password);

    if (!isValidPassword) {
      const error = new Error('El password no es válido');
      error.code = 401;
      throw error;
    }

    // 3. Construimos el JWT para enviárselo al cliente.
    const tokenPayload = { id: user.id };

    const token = jwt.sign(
      tokenPayload,
      process.env.SECRET,
      { expiresIn: '30d' },
    );
    
    res.send({ token });

  } catch (err) {
    next(err);
  }
}
```

De esta forma, cuando el usuario llame al endpoint de *login*, se comprobaría la combinación de email y password que está enviando en la request, y, si es correcta, se le enviaría un *token JWT* que podrá usar para realizar sus próximas peticiones al backend.

Así pues, el backend debe comprobar, para cada una de las peticiones (excepto la propia petición de *login*, en la cual el usuario enviará email y password) que se está enviando ese *JWT* dentro del *header* `Authorization`. Por lo general se usa la notación `Bearer ${token}` (donde token se refiere al JWT).

Como sabemos de temas anteriores, podemos usar *middlewares* para añadir lógica a cada una de las peticiones que se envían a un servidor *Express*. Así, podemos definir un *middleware* que se encarga de comprobar el *header* `Authorization` en cada petición, evitando que tengamos que escribir el código para validar el header en cada uno de nuestros controllers. Esta podría ser una implementación del *middleware* que se encarga de validar el header `Authorization` en cada request:

```javascript
async function validateAuthorization(req, res, next) {

 try {
  const { authorization } = req.headers;

  if (!authorization || !authorization.startsWith('Bearer ')) {
    const error = new Error('Authorization header required');
    error.code = 401;
    throw error;
  }

  const token = authorization.slice(7, authorization.length);
  const decodedToken = jwt.verify(token, process.env.SECRET);

  // Comprobamos que el usuario para el que fue emitido
  // el token todavía existe.
  const query = 'SELECT * FROM users WHERE id = ?';
  const [users] = await database.pool.query(query, id);

  if (!users || !user.length) {
    const error = new Error('El usuario ya no existe');
    error.code = 401;
    throw error;
  }

  req.auth = decodedToken;
  next();
 } catch (err) {
   next(err);
 }
}
```

Así, ya tendríamos aislada la lógica que comprueba el *JWT* del usuario en un único punto, y podríamos directamente configurar esta lógica como un *middleware* que podríamos delante de los endpoints a los que quisiésemos aplicárselo (las partes de nuestro backend que queremos proteger de tal forma que sólo los usuarios que estén dados de alta y tengan un token válido puedan acceder).

---

## Proyecto
### Parte 1
* Aplicación de gestión de reviews de restaurantes.
* La aplicación gestiona usuarios, restaurantes, y opiniones de los primeros sobre los segundos. Permite:
- Obtener un listado de restaurantes (visible sin autenticación).
- Que un usuario se registre en la aplicación. El cliente deberá enviar la password en dos properties del body por seguridad, `password` y `repeatedPassword`, y deberemos verificar que no existe otro user con el mismo email.
- Que un usuario se loguee en su cuenta. El cliente deberá enviar email y password para hacer login. Se le devolverá un token JWT que deberá enviar siempre para interactuar con el API, y su id de usuario en la respuesta.
- Que un usuario pueda crear una review. Enviará `rating` (número entre 1 y 5) y `text` en el body de la petición.
- Que un usuario pueda ver todas sus reviews mediante un endpoint que devuelva las reviews por `user_id`. No podrá ver las de resto de usuarios, es decir, si se pide un `user_id` diferente al del propio usuario, el API devolverá un error.
- Que un usuario pueda modificar sus reviews, pero no las de resto de usuarios.
- Obtener la puntuación media de un restaurante en base a las opiniones de los usuarios. Si el restaurante no tiene reviews todavía, deberemos devolver un `204 NO CONTENT`.

### Parte 2
* Vamos a añadir el concepto de *administrador*. Para hacerlo, vamos a ejecutar los cambios en la base de datos:
```sql
USE backend2_restaurants;

ALTER TABLE users ADD role VARCHAR(50) DEFAULT 'user';
```

Además de los cambios en la tabla `users`, deberemos crear los datos del administrador: 
```sql
INSERT INTO users (name, email, password, role) VALUES
  ('Administrador',
  'admin@restaurants-api.com',
  '$2a$10$uNY2e/48xjzjnZR9Vs5k6erkdOU9O9P0VrCSsYfglPdASCDwd46pa', /* hash para 'adminPass' */
  'admin');
```

* Un administrador, además de las acciones que pueden realizar el resto de usuarios, podrá:
- Obtener el listado de usuarios.
- Crear un nuevo usuario.
- Ver todas las reviews.
- Modificar todas las reviews.
- Borrar un usuario con todas sus reviews.
- Borrar un restaurante con todas sus reviews.

### Parte 3
* Vamos a añadir y modificar varios endpoints en el API:
- El usuario podrá subir imágenes de las reviews. Cada imagen se guardará en el directorio `static/images/{review_id}/{nombre_del_archivo}.{extension_del_archivo}`, donde `review_id` será el id númerico de la review, `nombre_del_archivo` será un campo generado por el backend usando `uuid`, y `extension_del_archivo` será la extensión original del archivo subido.
- Cuando un archivo se sube, la añade a una tabla en la base de datos, llamada `images`, por lo que habrá que ejecutar un script SQL en la base de datos para crear esa tabla. Los datos de esta tabla serán `id`, `review_id`, y `url` (el path a la imagen).
- La carpeta `static` del servidor va a ser pública, es decir, un cliente podrá pedir el contenido de una imagen llamando a la URL `directorio static/images/{review_id}/{nombre_del_archivo}.{extension_del_archivo}`.
- Modificaremos los endpoints de tipo GET a /api/review para que cada review contenga un array de objetos llamado `images` con la siguiente forma:
```json
{
        "id": ...,
        "user_id": ...,
        "restaurant_id": ...,
        "rating": ...,
        "text": ...,
        "images": [
            {
              "id": ${imageId},
              "url": "static/images/...", 
            },
            {
              "id": ${imageId},
              "url": "static/images/...", 
            },
            {
              "id": ${imageId},
              "url": "static/images/...", 
            }
        ]
    }
```
- Añadiremos un endpoint de tipo DELETE que nos permita eliminar imágenes por id. El usuario sólo podrá borrar la imagen si es dueño de la review a la que pertenece la imagen o es administrador.
- Añadiremos un endpoint de tipo DELETE que nos permita eliminar todas las imágenes de una review. El usuario sólo podrá borrar la review si es su dueño o es administrador.
