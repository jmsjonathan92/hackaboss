# Node.js Core Modules
**Node** trae por defecto una serie de módulos para realizar tareas comunes, en su mayoría relacionadas con el sistema, ficheros, red, etc... A estos módulos se les conoce como **core modules**. Podemos requerirlos en nuestro código en cualquier momento porque vienen incluídos en la instalación de **Node**.

Toda la información sobre los **core modules** de **Node** puede verse en la documentación oficial del proyecto.

Durante este curso nos centraremos en algunos de estos módulos, que son tal vez los más comunes:
* **os**: ofrece información proveniente del propio sistema operativo en el que corre nuestra aplicación **Node**.
* **path**: permite trabajar con rutas del sistema de ficheros.
* **fs**: permite trabajar con ficheros y directorios del sistema de ficheros.

### Módulo os
El módulo os nos permite conseguir información sobre el sistema operativo donde se está ejecutando el proceso de Node. Cosas como la memoria total, la memoria libre, el número, estado de los procesadores y otras cosas relacionadas.

```javascript
const os = require('os');

os.totalmem(); // total de memoria del sistema.
os.freemem(); // total de memoria libre.
os.hostname(); // nombre del host (máquina donde corre el proceso de Node).
os.homedir(); // la ruta del directorio personal del usuario que ejecuta el proceso de Node.
os.tmpdir(); // la ruta del directorio temporal para trabajar con archivos volátiles.
```

### Módulo path
El módulo path nos servirá para manejar y transformar *paths* (rutas) del sistema de ficheros. Es un módulo importante ya que las rutas en los diferentes sistemas operativos no tienen un formato común y este módulo nos garantiza que nuestra aplicación funciona bien en cualquier sistema.

```javascript
const path = require('path');

path.join(x); // construye un path por partes de forma segura e independiente del sistema operativo.
path.resolve(x); // combina una serie de paths para convertirlos en un path absoluto.
path.normalize(x); // normaliza un path.
path.extname(x); // nos dará la extensión del fichero apuntado por el path.
```

Además, el módulo **path** nos sirve para componer rutas usando `path.join`. Por ejemplo, para componer la ruta al archivo `mi_archivo.txt` dentro del directorio actual, podemos hacer uso de `path.join` en conjunción con la variable global `__dirname` de **Node**.

```javascript
const path = require('path');

const myPath = path.join(__dirname, 'mi_archivo.txt')
```

## Módulo fs
Será el **core module** de Node que más usaremos, y nos permitirá interactuar con el sistema de ficheros. Nos permite leer, crear, escribir o borrar ficheros y directorios.

Las funciones que proporciona este módulo normalmente tienen dos versiones, una asíncrona y otra síncrona. Intentaremos usar siempre la asíncrona porque proporciona un mejor rendimiento. En versión asíncrona ejecutaremos un método y le pasaremos un callback que se ejecutará cuando acabe de realizar la tarea. Vemos varias versiones equivalentes:

```javascript
const fs = require('fs');

fs.writeFile('file.txt', 'Hello world!', function(err) {
  if (err) {
    console.error(err);
  }
  
  console.log('El fichero se escribió correctamente');
});
```

```javascript
const fs = require('fs');

fs.writeFile('file.txt', 'Hello world!', (err) => {
  if (err) {
    console.error(err);
  }
  
  console.log('El fichero se escribió correctamente');
});
```

```javascript
const fs = require('fs');

const callback = (err) => {
  if (err) {
    console.error(err);
  }
  
  console.log('El fichero se escribió correctamente');
};

fs.writeFile('file.txt', 'Hello world!', callback);
```

```javascript
const fs = require('fs');

const callback = err => err ? console.error(err) : console.log('El fichero se escribió correctamente');

fs.writeFile('file.txt', 'Hello world!', callback);
```

En estos fragmentos estamos usando la versión asíncrona de la función `writeFile`. La ejecución asíncrona permite que Node siga ejecutando el resto del código mientras `writeFile` se ejecuta en paralelo, es decir, Node no espera a que `writeFile` termine su ejecución. Esto como hemos dicho mejora mucho el rendimiento en aplicaciones complejas, ya que permite paralelizar tareas que requieren acceso al disco duro, a servidores externos, etc.

La versión síncrona de estas funciones suele tener el sufijo `Sync`. No usaremos las versiones síncronas durante el curso, pero pueden ser útiles para probar código rápidamente en el REPL. Ejemplo:

```javascript
const fs = require('fs');

fs.writeFileSync('file.txt', 'Hello world!');
```

Volviendo a las funciones asíncronas, es importante notar que los callbacks son funciones corrientes. Para hacer nuestro código más legible y no [encadenar múltiples callbacks unos dentro de otros](https://i.ytimg.com/vi/fr67u98nckk/maxresdefault.jpg), en las últimas versiones de Node, ya podemos hacer uso del patrón **Promise** de JS en la mayoría de **core modules**, por ejemplo, para el módulo **fs**:

```javascript
const fs = require('fs').promises;

async function readFile(filename) {
  const data = await fs.readFile(filename);

  return console.log(data.toString());
}

readFile('file.txt'); // file.txt existe en el sistema de ficheros
```

Esto nos facilita también la gestión de errores, ya que podemos hacer uso de bloques `try-catch`, en lugar de estar comprobando si el callback retorna un argumento `error` como hacíamos en los ejemplos anteriores.

```javascript
const fs = require('fs').promises;

async function readFile(filename) {
  try {
    const data = await fs.readFile(filename);

    return console.log(data.toString());
  } catch (err) {
    console.error(`error reading file ${filename}`);
  }
}

readFile('fil3.txt'); // fil3.txt no existe en el sistema de ficheros
```

Vemos que para leer el contenido de un fichero estamos siempre usando el método `toString()`. Esto se debe a que por defecto los ficheros se leen como `Buffer`, es decir, se leen como cadenas de código binario. Otra opción para obtener el contenido de un fichero como una cadena de caracteres es indicar el formato esperado, por ejemplo:

```javascript
const fs = require('fs').promises;

async function readFile(filename) {
  const data = await fs.readFile(filename, 'utf8');

  return console.log(data.toString());
}

readFile('file.txt');
```

A partir de ahora, usaremos siempre que sea posible **Promises** para trabajar con el módulo **fs**.

---

### Escribiendo ficheros
```javascript
const fs = require('fs').promises;

async function myFunction() {
  try {
    await fs.writeFile('mensaje.txt', 'Hola, mundo!');
  } catch (err) {
    console.error(err);
  }
}

myFunction();
```

---

### Leyendo ficheros
```javascript
const fs = require('fs').promises;

async function myFunction() {
  try {
    const data = await fs.readFile('mensaje.txt', 'utf-8');
    console.log(data);
  } catch (err) {
    console.error(err);
  }
}

myFunction();
```

---

### Borrando ficheros
```javascript
const fs = require('fs').promises;

async function myFunction() {
  try {
    await fs.unlink('mensaje.txt');
  } catch (err) {
    console.error(err);
  }
}

myFunction();
```

---

### Ver metadata de un fichero
```javascript
const fs = require('fs').promises;

async function myFunction() {
  try {
    const data = await fs.stat('mensaje.txt', 'utf-8');
    console.log(data);
  } catch (err) {
    console.error(err);
  }
}

myFunction();
```

Existen otras muchas funciones en el módulo **fs**, como siempre, podemos consultar la documentación oficial para conocerlas. 

---

### Tratamiento de errores
Todas las funciones del módulo **fs** pueden tirar errores, por eso estamos haciendo las llamadas en nuestro código envueltas en una cláusula `try-catch`. Algunos de los erroes más comunes con **fs** pueden consultarse [en la documentación](https://nodejs.org/api/errors.html#errors_common_system_errors).

Por ejemplo, pongámonos en el caso de que intentamos borrar un fichero que no existe. En ese caso Node nos lo indicará tirando el error `ENOENT`. Para manejar ese caso y evitar que nuestra aplicación termine su ejecución con un error, podemos hacer un tratamiento del error dentro del bloque `catch`. Por ejemplo, podemos imprimir el mensaje de error por pantalla y seguir con la ejecución. 

Esto es sólo un ejemplo, por lo general cuado ocurre un error no sólo querremos imprimirlo por consola, sino que querremos hacer algún tipo de lógica con él (informar al administrador, generar un reporte en el servidor, enviar una alerta a otro sistema, etc.)

```javascript
const fs = require('fs').promises;

async function deleteFile(filename) {
  try {
    await fs.unlink(filename);
  } catch (err) {
    if (err.code === 'ENOENT') {
      console.error('No se encontro el archivo');
    } else {
      throw err;
    }
  }
}

deleteFile('fil3.txt');
```

---

## Bonus track: Trabajando con JSON en Node
**JSON** es el formato más habitual en el que guardar datos.

Como ya sabemos, para leer un archivo se puede usar el módulo **fs**, por ejemplo, imaginemos que tenemos un archivo que se llama `provincias.json` y cuyo contenido es:

```javascript
{
  "provincias": ["lugo", "coruña", "ourense", "pontevedra"]
}
```

```javascript
const fs = require('fs').promises;

async function myFunction() {
  try {
    const data = await fs.readFile('provincias.json', 'utf-8');
    console.log(data);
  } catch (err) {
    console.error(err);
  }
}

myFunction();
```

Para facilitar el tratamiento de **JSON** en **Node** tenemos una manera más fácil de leer estos archivos:

```javascript
const provincias = require('./provincias.json');

console.log(provincias);
```

Como sabemos, `JSON.parse` procesa strings y los convierte en objetos JS. `JSON.stringify` hace exactamente lo contrario, así pues, también podemos usarlo para escribir objetos JS en ficheros:

```javascript
const fs = require('fs').promises;

const myFunction = async () => {
  const dimensions = { height: 300, width: 200 };

  try {
    await fs.writeFile('dimensions.json', JSON.stringify(dimensions))
  } catch (err) {
    console.error(err);
  }
}

myFunction();
```

---

### Ejercicio 1
* Usando el módulo `fs` de NodeJS, muestra por pantalla el contenido de file.txt. Puedes usar callbacks o promises, aunque lo recomendable es lo segundo.
* **Pista**: el contenido se lee como un Buffer de datos, usa UTF-8 para leerlo.

### Ejercicio 2
* Usando el módulo `fs` de NodeJS, escribe una función que acepte como argumentos dos strings. El primer argumento representará un nombre de archivo y el segundo un contenido a escribir en el archivo.
* Llama a esta función para escribir un fichero `file.txt` en el directorio actual el contenido `Hola Backend!`.
* **Pista**: Puedes usar callbacks o promises, aunque lo recomendable es lo segundo.

### Ejercicio 3
* Usando el módulo `fs` de NodeJS, escribe una función que acepte como argumento un *string*.
* La función deberá imprimir el string `Es un directorio` si el `Path` que le pasamos es un directorio.
* La función deberá imprimir el string `Es un archivo` si el `Path` que le pasamos es un archivo.
* La función deberá imprimir el string `No existe` si el `Path` que le pasamos no existe.
* **Pista**: Puedes usar callbacks o promises, aunque lo recomendable es lo segundo.

### Ejercicio 4
* Usando el módulo `fs` de NodeJS, escribe una función que acepte como argumento un nombre de archivo.
* La función deberá borrar ese archivo del directorio actual.
* La función deberá imprimir el string `No existe` si el nombre de archivo que le pasamos no existe.
* Si se produce algun error, deberá imprimir el error.
* **Pista**: Puedes usar callbacks o promises, aunque lo recomendable es lo segundo.

