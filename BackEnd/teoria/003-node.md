# Introducción a Node.js
**NodeJS** es un entorno de ejecución JS en el servidor.
* Está basado el motor V8 liberado por Google, lo que permite el uso de JavaScript en el servidor
* Por defecto, corre en un único proceso, haciendo uso del **Event Loop**, un sistema que permite I/O sin bloqueos sin recurrir al uso de threads (compleja gestión de memoria, condiciones  de carrera, etc.)
* Es ideal para que los equipos de desarrollo trabajen en un único lenguaje en frontend y backend, lo que también facilita e impulsa la figura del desarrollador *full-stack*.
* Al ser JS un lenguaje de tipado dinámico e interpretado, usando el Node REPL podemos probar código JS de manera muy fácil para el programador. Para usar el REPL, simplemente ejecutamos el comando `node`
* Para ejecutar un módulo de Node, simplemente escribimos node seguido del nombre del fichero.

---

## Primer servidor web usando Node.js
Escribir un servidor funcional en Node.js es tan sencillo como esto:
```javascript
const http = require('http');

const hostname = '127.0.0.1';
const port = 3000;

const server = http.createServer((req, res) => {
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/plain');
  res.end('Hello World');
});

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});
```

---

## Ejecución de código en Node.js
### Ejecución usando el REPL
La forma más sencilla de probar Node.js en nuestra máquina es ejecutar directamente el **REPL** (Read-Eval-Print-Loop) que Node.js incorpora. Esta funcionalidad nos permite ejecutar código JavaScript directamente en la línea de comandos. Para abrir un **REPL**, una vez instalado Node.js en nuestra máquina, simplemente tecleamos `node`. A partir de ese momento podremos ejecutar código JavaScript directamente.
```javascript
const name = 'Héctor';
console.log(`¡Bienvenido, ${name}!`);
```
Vemos que después de la ejecución de la orden, el **REPL** muestra un `undefined`, esto es debido a que línea que hemos ejecutado no ha devuelto ningún valor (sólo ha impreso un string por pantalla). Si por ejemplo ejecutamos:
```javascript
const a = 2;
const b = 3;
Math.pow(a, b);
```
Como vemos la ejecución de la última línea imprime `8` por pantalla, en lugar de `undefined` (que sí se imprimirá en cada definición de variable).
Para salir de este REPL debemos presionar Control-C dos veces.

### Ejecución de ficheros
Aunque ejecutar código usando el **REPL** es una forma fácil de probar cosas, resolver dudas, y jugar con Node.js, la forma más habitual de trabajar y como funcionarán nuestros servidores en producción es ejecutando archivos que contienen código JavaScript. Por ejemplo, podemos usar nuestro editor favorito para crear un fichero que contenga el código del ejemplo anterior:
```javascript
const a = 2;
const b = 3;
Math.pow(a, b);
```
ahora podemos usar Node.js para ejecutar el fichero que hemos guardado en nuestra máquina con nombre pow.js.
```
node pow.js
```
Vemos que el archivo se ejecuta, pero no se muestra nada por pantalla. No ha habido ningún error y nuestro código se ha ejecutado perfectamente, pero aunque se han declarado variables y se ha hecho la operación, no hay ninguna orden que indique a Node que tiene que mostrar algo por pantalla. Podemos modificar nuestro código para que el ejemplo sea un poco más visual:
```javascript
const a = 2;
const b = 3;
const result = Math.pow(a, b);
console.log(`El resultado es: ${result}`);
```
Y ahora sí deberíamos ver:
```
El resultado es: 8
```
Nota: aunque en estos ejemplos hemos usando `node mi_fichero.js`, podríamos haber usado simplemente `node mi_fichero`. La extensión `.js` está implícita cuando ejecutamos archivos con Node.

---

## Sistema de módulos en Node.js
* Node.js usa el sistema modular CommonJS (ES6 Modules sólo experimentalmente)
* En cada fichero definiremos un módulo, que se encargará de una pequeña parte del código de nuestro servidor.
* Exportaremos ciertas partes de nuestros módulos al resto de módulos, de tal forma que puedan complementar sus tareas y consigamos separar lógicamente nuestro código
* Para ello usaremos el objeto `module.exports`
* Importaremos funcionalidades de otros módulos usando `require`
* Además de los módulos propios, Node.js expone una serie de módulos que nos permiten interactuar con el sistema operativo y también otros con utilidades. Estos son los que se conocen como **core modules** y algunos los veremos más en profundidad (**http** y **fs**, por ejemplo). Estos **core modules** también se importan mediante `require`.
### Definición de módulos en Node.js
Como hemos dicho, existen numerosos módulos que Node.js pone a nuestra disposición para facilitarnos el desarrollo. Estos módulos cubren tareas comunes y ahorran al programador tener que reescribir utilidades comunes que se necesitan en cualquier servidor web. Tal es el caso de módulos de uso general como *http*, *fs*, *path*, *crypto*, etc. Estos módulos los veremos más en profundidad próximamente. Ahora vamos a ver cómo definir nuestros propios módulos, evitando así tener que reescribir código y aislando una funcionalidad en un determinado sitio. Por ejemplo, pongamos que tenemos que calcular la suma de los elementos de un array de números que sean mayores que 3. Para ello definimos una función que recibe el array como parámetro y devuelve dicha suma:
```javascript
function arraySum(arr) {
  return arr.filter(i => i > 3).reduce((sum, i) => sum + i);
}
```
O, lo mismo, usando **arrow functions**:
```javascript
const arraySum = arr => arr.filter(i => i > 3).reduce((sum, i) => sum + i);
```
Definir esta funcionalidad en un módulo nos permite aislarla del resto del código, de tal forma que simplemente importándola en otro punto de nuestro programa podemos usarla sin tener que reescribirla. Así pues, vamos a *exportarla*. Para ello, metemos la referencia a esta función dentro del objeto `module.exports` de nuestro archivo. 
```javascript
const arraySum = arr => arr.filter(i => i > 3).reduce((sum, i) => sum + i);

module.exports = { arraySum };
```
Esto hará que `arraySum` sea una función pública, es decir, podemos importarla desde otro punto de nuestro código usando `require`. Así, por ejemplo, imaginemos que yo guardo ese código en un archivo que se llama `sums.js`, y ahora guardo este otro código en el archivo `index.js` dentro del mismo directorio:
```javascript
const { arraySum } = require('./sums');

const array = [1, 2, 3, 4, 5, 6, 7];
const result = arraySum(array);
console.log(`El resultado es ${result}.`);
```
Ahora puedo ejecutar el archivo `index.js` y ver que se está haciendo la operación mediante la ejecución de una función que está definida en otro archivo distinto al que estoy ejecutando.
Nótese que:
* He importado directamente la función `arraySum` mediante destructuring, podría haber importado todo el módulo con `const sums = require('./sums');`. En ese caso debería invocar a la función con `sums.arraySum(array);`.
* He omitido la extensión `.js` en el `require`. Al igual que el **REPL**, se entiende que cuando no se especifica extensión, ésta es `.js`.
* El require hace referencia a un archivo que está en la misma carpeta que el que se está ejecutando. En otro caso, debería especificar la ruta. Por ejemplo si estuviese en una carpeta que se llamase **utils**, tendría que escribir `const { arraySum } = require('./utils/sums');`

---

## Variables globales
Node.js pone a disposición pocos identificadores globales pero algunos muy importantes:
* `process`: este objeto global contiene información sobre el proceso de Node.js que se está ejecutando. Muy útil para trabajar con las variables de entorno (veremos esto más adelante) y para gestionar la ejecución del propio proceso.
* `console`: nos permite escribir información en el output del proceso. Dentro de este objeto hay varios objetos que nos permite escribir por diferentes **salidas**, por ejemplo `console.log`, `console.warn` o `console.error`.
* `setTimeout`, `clearTimeout`, `setInterval`, `clearInterval`: para crear y controlar **timers**.
* `module`: como ya hemos visto, es una referencia al módulo (fichero) y nos permitirá exportar objectos mediante module.exports.
* `__filename`: contiene la ruta completa del fichero.
* `__dirname`: contiene la ruta completa del directorio donde está el fichero.
* `require`: como ya hemos visto, esta función nos permite incluir otros módulos.

---

### Eventos
Podemos usar `process` para asignar eventos al proceso que se está ejecutando, por ejemplo, si queremos hacer algo antes de que el proceso de node se pare podemos asignar una función que se ejecute justo antes de parar:

```javascript
process.on('exit', function () {
  console.log('El proceso de node va a parar');

  // Aquí podríamos cerrar la conexión a la base de datos
  // escribir en disco algún dato importante, etc.
});
```

También podemos hacer que ejecute algo cuando ocurra un error que no tengamos controlado con un bloque `try-catch`, que de otra forma haría que el proceso de node se interrumpiera:

```javascript
process.on('uncaughtException', function (err) {
  console.error('Ocurrió un error grave!');
  console.error(err.stack);
  
  // De esta forma el proceso de node continuará pero mostrará el error en el terminal.
});
```

Por ejemplo, si creamos un archivo que contenga este código y lo ejecutamos:
```javascript
process.on('exit', () => console.log('El proceso de node va a parar'));

process.on('uncaughtException', function (err) {
  console.error('Ocurrió un error grave!');
  console.error(err.stack);
});

throw new Error('Este es el mensaje de error');
```

Vemos que se muestra por pantalla algo parecido a esto:
```
Ocurrió un error grave!
Error: Este es el mensaje de error
    at Object.<anonymous> (/Users/hectorgv/projects/hackaboss/delete/start.js:8:7)
    at Module._compile (internal/modules/cjs/loader.js:1063:30)
    at Object.Module._extensions..js (internal/modules/cjs/loader.js:1092:10)
    at Module.load (internal/modules/cjs/loader.js:928:32)
    at Function.Module._load (internal/modules/cjs/loader.js:769:14)
    at Function.executeUserEntryPoint [as runMain] (internal/modules/run_main.js:72:12)
    at internal/main/run_main_module.js:17:47
El proceso de node va a parar
```
Como vemos Node.js ha detectado que se ha producido un error (lo hemos tirado nosotros mismos usando `throw`), por lo que ha ejecutado el código que hay definido para el **listener** que maneja el evento `uncaughtException`, y después, antes de salir de la ejecución, ha ejecutado el código que hay definido para el **listener** del evento `exit`. Más información sobre eventos y como manejarlos en la documentación oficial: [Events](https://nodejs.org/docs/latest-v14.x/api/events.html#events_emitter_on_eventname_listener)

---

### Variables de entorno

Mediante el objeto `process.env`, podemos acceder a las variables de entorno del proceso de Node que se está ejecutando. Estas variables de entorno, por lo general, almacenarán información que es crítica en nuestros servidores Node.js. Por ejemplo, la cadena de conexión a una base de datos, el secreto para verificar tokens de usuario, etc. por lo general serán definidas usando variables de entorno.

Por ejemplo, para imprimir todas las variables de entorno por pantalla (jamás deberíamos hacer esto puesto que suelen contener información sensible):
```javascript
console.log(process.env)
```
Para imprimir el contenido de una variable en concreto, por ejemplo `PATH`:
```javascript
console.log(process.env.PATH)
```

Por lo general, no querremos imprimir, ni modificar, las variables de entorno en nuestras aplicación Node.js. La mayoría de veces lo que vamos a necesitar es coger información de estas variables de entorno para configurar nuestro servidor. Por ejemplo, volviendo al código que teníamos al principio de la sección, donde se definía un primer servidor Node, imaginemos que los valores para el hostname y el puerto los ha configurado el administrador de sistemas en unas variables de entorno que se llaman `HOST` y `PORT`. En ese caso, para respetar sus configuraciones, vamos a usar esos valores para iniciar nuestro servidor Node, leyéndolos de `process.env`:
```javascript
const http = require('http');

const hostname = process.env.HOST;
const port = process.env.PORT;

const server = http.createServer((req, res) => {
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/plain');
  res.end('Hello World');
});

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});
```
O usando destructuring:
```javascript
const http = require('http');

const { HOST, PORT } = process.env;

const server = http.createServer((req, res) => {
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/plain');
  res.end('Hello World');
});

server.listen(PORT, HOST, () => {
  console.log(`Server running at http://${HOST}:${PORT}/`);
});
```

---

### Argumentos de entrada
Ya hemos visto que los comandos en **Bash** aceptan una serie de argumentos. Por ejemplo cuando ejecutamos `node app.js 33 hello world` le estamos pasando al comando `node` los argumentos `app.js`, `33`, `hello` y `world`.

Podemos acceder a esos argumentos en Node usando el objeto `process.argv` que es un *array* que contiene todos los argumentos con los que se ejecutó el proceso actual, siendo los dos primeros el propio path del ejecutable de node y el path del script (`app.js` en este caso). Podemos verlo si creamos un fichero `app.js` con estos contenidos:
```javascript
console.log('Imprimiendo los argumentos de entrada:');
console.log(process.argv);
```

Y ahora lo ejecutamos:
```
node app.js hola que tal
```
Imprimirá algo parecido a:
```
[
  '/usr/local/bin/node',
  '/Users/hectorgv/projects/hackaboss/delete/start.js',
  'hola',
  'que',
  'tal'
]
```

---

### Ejercicio 1
* Crea un módulo `index` que simplemente muestre por pantalla la primera palabra que se le pasa como primer argumento cuando se ejecuta este módulo desde bash.

### Ejercicio 2
* Crea un módulo `simple-maths` que exponga una función `double(x)`, que calcula el doble de un número que se le pasa como argumento.
* Crea otro módulo `index` que hace uso del anterior. Este módulo usará como entrada el primer argumento que se le pasa a la ejecución del módulo, y se lo pasará al módulo `simple-maths`, quien calculará el doble del número, e `index` se encargará de imprimir el resultado por pantalla.
* **Pista 1**: los valores almacenados en `argv` son siempre considerados *strings*, por lo que deberemos hacer la conversión a número con `Number(x)`.

### Ejercicio 3
* Modifica el ejercicio anterior para que cuando el argumento que le llega a la función `double(x)` del módulo `simple-maths` no sea un número, se tire un error.
* Modifica el módulo `index` para capturar el error y mostrarle al usuario el mensaje `El argumento debe ser un número` por pantalla.
* **Pista 1**: Hay varias formas de comprobar si una variable contiene un número en JS, las más fáciles de usar son usar `Number.isFinite(x)` o `Number.isNaN(x)`.
* **Pista 2**: puedes usar `throw new Error()` para tirar un error, y un bloque `try-catch` para capturarlo.