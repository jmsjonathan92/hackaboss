'use strict'

import { getData, shuffleArray, randomItem } from "./helpers.js";

const STATE = {
    article: [],
    points: 0,
};
const counterElement = document.querySelector('.counter')
const questionElement = document.querySelector('.question')

// Esta funcion genera respuesta del mismo grupo
async function getArticleCategories(article) {
    const data = await getData(`https://es.wikipedia.org/w/api.php?format=json&origin=*&action=query&prop=categories&titles=${article}
    `);

    const key = Object.keys(data.query.pages)[0];

    const categories = data.query.pages[keys].categories
        .map((item) => item.title)
        .filter((item) => !item.includes("Wikipedia"));

    return categories;
}

async function getCategoryArticles(category) {
    const data = await getData(`https://es.wikipedia.org/w/api.php?format=json&origin=*&action=query&list=categorymembers&cmlimit=500&cmtitle=${category}
    `)

    const articles = data.query.

    return articles;
}

// Esta funcion genera el texto y oculta la respuesta
async function generateCuestion() {
    try {
        questionElement.innerHTML = 'Cargando...';

        // Escogemos un articulo al azar
        const answer = randomItem(STATE.articles);

        // Saco informacion del articulo escogido
        const { displaytitle, extract } = await getData(
            `https://es.wikipedia.org/api/rest_v1/page/summary/${answer}`);

        // Remplazo en el resumen el texto del titulo por ▇▇▇▇▇▇▇
        const HIDDEN = "▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇";
        const replacement = new RegExp(displaytitle, "ig");
        const clue = extract.replaceAll(replacement, HIDDEN);

        // Si la pista creada no incluye la respuesta genero un error
        if (!clue.includes(HIDDEN)) {
            throw new Error('Cannot fin title in clue');
        }

        // Estraemos las categorias interesantes del articulo
        const getArticleCategories = await getArticleCategories(answer);

        // escojo una categoria al azar
        const chosenCategory = randomItem(articleCategories);

        // Extraigo los articulos de la categoria escogida
        const chosenCategoryArticles = await getCategoryArticles(chosenCategory);

        //Saco la respues correcta de los Articulos
        const validCategoryArticles = chosenCategoryArticles.filter(
            (item) => item !== displaytitle && !item.includes(":")
        );

        // Si me quedan menos de 3 articulos en la lista mando un error y se vuelve a ejecutar
        if(validCategoryArticles.length < 3) {
            throw new Error("Not enough options")
        }

        // Saco 3 articulos de la lista que seran las opciones falsas
        const falseOptions = shuffleArray(validCategoryArticles).slice(0, 3);

        // Creo un obejto que define a la preguna: pista, respuesta y opciones
        const question = {
            clue,
            answer: displaytitle,
            ooptions: shuffleArray([...falseOptions,displaytitle]),
        };

        writeQuestion(question);

    } catch (error) {
        console.error(error);
        generateCuestion();
    }
}


function writeQuestion(question) {
    // Borramos tood lo que hay en el elemento de la pregunta
    questionElement.innerHTML = "";

    // Escribimos la Pista
    const clueElement = document.createElement('p');
    clueElement.innerText = question.clue;

    questionElement.append(clueElement);

    // Escribimos las respuestas
    for (const answer of question.options) {
        const answerElement = document.createElement('button');
        answerElement.innerText = answer;

        answerElement.onclick = () => {
            if(answer === question.answer){
                alert('correcto');
                STATE.points++;
            } else {
                alert('No es correcto. Vuelves a empezar');
                STATE.points = 0;
            }

            // Actualizamos puntuacion
            counterElement.innerText = STATE.points;

            generateCuestion();
        };

        questionElement.append(answerElement);
    }
}



// Esta funcion va a cargar todos los articulos más leidos de la wikipedia
async function start() {
    try{
    // creamos la fecha de ayer
    const yesterday = new Date(Date.now() - 86400000);

    // Extaemos el año, mes y dia (formateado correctamente)
    const year = yesterday.getFullYear();
    const month = String(yesterday.getMonth() + 1).padStart(2, "0");
    const day = String(yesterday.getDate()).padStart(2, "0");
    const topArticlesURL = `https://wikimedia.org/api/rest_v1/metrics/pageviews/top/es.wikipedia.org/all-access/${year}/${month}/${day}`
    const topArticles = await getData(topArticlesURL);
    const articlesTittle = topArticles.items[0].articles.map((item) => item.article);
    const validArticles = articlesTittle.filter((item) => !item.includes(":"));

    STATE.articles = validArticles;

    generateCuestion();

    } catch (error) {
        console.error("Desactiva el adbblock.");
    }
}

start();