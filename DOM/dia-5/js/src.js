'use strict';

const ul = document.querySelector('ul.users');
const updateButton = document.querySelector('button.update');
const sendButton = document.querySelector('button.send');
const form = document.querySelector('form');

const Users = [
    {
        name: 'Paco',
        age: '34',
    },
    {
        name: 'Laura',
        age: 23,
    },
];

// Almacenamos en el localStorage los usuarios pero antes los convertimos a texto
// JSON.
window.localStorage.setItem('users', JSON.stringify(Users));

// Botón que agrega la lista de usuarios como li en el documento.
updateButton.addEventListener('click', () => {
    // Eliminamos el contenido del ul.
    ul.innerHTML = '';

    // Recuperamos un valor del localStorage y lo reconvertimos a un objeto
    // JavaScript.
    const usersCopy = JSON.parse(window.localStorage.getItem('users'));

    const frag = document.createDocumentFragment();

    for (const user of usersCopy) {
        const li = document.createElement('li');

        li.textContent = `Soy ${user.name} y tengo ${user.age} años`;

        frag.append(li);
    }

    ul.append(frag);
});

// Agregamos al botón send un event listener que pushea usuarios en el
// array.
sendButton.addEventListener('click', (e) => {
    e.preventDefault();

    Users.push({
        name: form.elements.name.value,
        age: form.elements.age.value,
    });

    alert('¡Usuario registrado!');

    window.localStorage.setItem('users', JSON.stringify(Users));
});
