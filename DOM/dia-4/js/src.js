'use strict';

/* #################
   #### EVENTOS #### 
   #################
*/

// Primer evento. Pulsar un boton (PRIMERA FORMA) DE ESTA FORMA JAMAS SE PODRÁ ELIMINAR
const button = document.querySelector('button');

let count = 0;

/* button.onclick = function () {
    console.log('Acabas de hacer click en el botón');
}; */

button.onmouseover = function () {
    console.log('acabas de pasar el ratón por encima');
};

button.onfocus = function () {
    console.log('Foco sobre el botón');
};

// (SEGUNDA FORMA)

button.addEventListener('click', function () {
    console.log('Click en el boton');
});

// otra forma de hacerlo es así, de está forma podemos eliminarlo guardando el colback

const handleButonMouseOver = () => {
    console.log('Has pasado el raton por encima');
};

button.addEventListener('mouseover', handleButonMouseOver);

// PARA ELIMINAR UN EVENTO SERIA
setTimeout(() => {
    button.removeEventListener('mouseover', handleButonMouseOver);
}, 10000);
// se podría hacer sin el setTimeout, es por practicar.
