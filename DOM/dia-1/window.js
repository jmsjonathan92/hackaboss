'use strict';

// Altura del área del contenido de la ventana en px
console.log(window.innerHeight);

// Ancho del área del contenido de la ventana en px
console.log(window.innerWidth);

// Altura de la ventana pero al completo
console.log(window.outerHeight);

//Anchura de la ventana pero al completo
console.log(window.outerWidth);

// Mostrar el objeto location
console.log(window.location);
// con esto podremos redireccionar a los usuarios console.log(window.location = 'https://google.com');

// mostar el scroll horizontal y vertical en px
console.log(window.scrollX, window.scrollY);

// objeto console
console.log(window.console);

// Abrimos una nueva pestaña y guardamos su refrencia en una variable
const google = window.open('https://google.com');

// Podemos usar la referencia anterior para cerrar la pestaña
setTimeout(() => google.close(), 3000);

// Abrir la ventana de impresion:
console.log(window.print());
