'use strict';

// ### VAMOS A CRAER UN COLOR ALEATORIO EN EL HTML Y QUE CAMBIE CADA SEGUNDO

// seleccionas el parrafo del body
const p = document.querySelector('body > p');

// Seleccionamos el body por destructuring, tambien podriamos hacer como arriba
const { body } = document;

// Inicio un contador
let seconds = 0;

// background-color: vamos a crear un color aleatorio
function getRandom(max) {
    return Math.floor(Math.random() * (max + 1));
}

// Creamos un intervalo, podemos poner el window o no
window.setInterval(() => {
    // Cambiamos el color d fondo aleatoriamente
    body.style.backgroundColor = `rgb(${getRandom(255)}, ${getRandom(
        255
    )}, ${getRandom(255)})`;

    // cambiamos el contenido del parrafo
    p.textContent = seconds++;
}, 1000);
