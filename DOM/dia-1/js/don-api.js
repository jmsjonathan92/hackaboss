'use strict';

// Seleccionamos un modo por ID
const secondLi = document.getElementById('dos');

console.log(secondLi);

// seleccionando un unico nodo de una forma diferente
const body = document.querySelector('body');

// Con el metodo querySelector podemos utilizar cualquier selector de CSS
const header = document.querySelector('#principal');

// seleccion de multiples modos por clase
const miClaseElements = document.getElementsByClassName('miClase');

// Seleccionar multiples elementos
const allLi = document.querySelectorAll('Li');

console.log(allLi);

// seleccionamos el modo html
const html = document.querySelector('html');

// Seleccionamos los hijos del html
console.log(html.children);

// seleccionamos el primer hijo de html
console.log(html.firstElementChild);

// ultimo hijo
console.log(html.lastElementChild);

// Seleccionamos el body con la propiedad lastElementChild del html
const myBody = html.lastElementChild;

console.log(myBody);

const lis = document.querySelectorAll('li:nth-child(odd)');
for (const li of lis) {
    li.style.backgroundColor = 'lightcoral';
}
console.log(lis);

console.log(`
    ##################################################
    ###### MODIFICAMOS EL TEXTO DE LOS ELEMENTOS #####
    ##################################################
`);

/* Lo primero es seleccionar el nodo. Una vez hayamos guardado la referencia
de un nodo podemos cambiar su propiedad textContent para modificar su contenido
NO es posible inclir nuevas etiquetas html con esta propiedad */
secondLi.textContent += ', Adiós';

console.log(`
    ##################################################
    ###### MODIFICAMOS EL HTML DE LOS ELEMENTOS #####
    ##################################################
`);

const liName = 'Uno';

// La propiedad innerHTML si permite introducir nuevas etiquetas
/* body.innerHTML = ` 
    <header>
        <h1>Tope guay</h1>
    </header>
    <main>
        <ul>
            <li>${liName}</li>
            <li>Dos</li>
        </ul>
    </main>
`; */

console.log(`
    ##################################################
    ###### Conociendo y modificando atributos de los elementos #####
    ##################################################
`);

const button = document.querySelector('button');

// Obtenemos el valor de un atributo del noodo
console.error(body.getAttribute('lang'));

// Para crear un atributo que deshabilite el botón.
button.setAttribute('disabled', true);

// Comprobamos si un nodo tiene un atributo
console.log(body.hasAttribute('lang'));

// eliminar un atributo
button.removeAttribute('disabled');

console.log(`
    ##################################################
    ###### Modificando el CSS de los elementos #####
    ##################################################
`);

// seleccionamos el estilo backgroundColor del body y lo modificamos
body.style.backgroundColor = 'blue';

// Si queremos agregar varias propiedas de una sola vez
secondLi.style.cssText = `
    color: white;
    background-color: darkblue;
`;

// agregando varias propiedades con setAttribute
secondLi.setAttribute('style', 'color: pink', 'background-color: black');

console.log(`
    ##################################################
    ###### Modificando las clases CSS de los elementos #####
    ##################################################
`);

// agregar una clase a un elemento o modo
body.classList.add('principal');

// elminar una clase
body.classList.remove('miClase');

// Si existe la clase la borra y si no existe la crea
body.classList.toggle('principal'); // lo volvemos hacer por que como existe la borra
body.classList.toggle('principal');

// Comprobamos si existe una clase
console.log(body.classList.contains('principal'));
