/**
 * Ejemplo de estructura de un li (una tarea) en el HTML:
 *
 * <ul class="todo-list">
 *
 *      <li data-index="0">
 *          <input type="checkbox" />
 *          <p>Texto de la tarea</p>
 *          <time datetime="fecha-en-iso">18/11/2020 - 21:47</time>
 *      </li>
 *
 * </ul>
 */

/**
 * ##################################################
 * ##  Importa las funciones y objetos necesarios  ##
 * ##################################################
 */

import State, {
    addTodo,
    toggleTodo,
    cleanTodos,
    deleteAllTodos,
} from './state.js';

/**
 * ######################################
 * ##  Selección de nodos / elementos  ##
 * ######################################
 *
 *  - Selecciona el formulario.
 *
 *  - Selecciona el ul.
 *
 *  - Selecciona el botón que se encargará de limpiar las tareas marcadas
 *    como completadas.
 *
 *  - Selecciona el botón que se encargará de vaciar las tareas.
 */

const form = document.querySelector('form.todo-form');
const todoList = document.querySelector('ul.todo-list');
const todoClean = document.querySelector('button.todo-clean');
const todoEmpty = document.querySelector('button.todo-empty');

/**
 * ##############################
 * ##  Enviar una nueva tarea  ##
 * ##############################
 *
 * Agregamos una función manejadora al evento submit del formulario que se
 * encargue de agregar una nueva tarea con los datos que el usuario introdujo
 * en el input.
 *
 * Renderiza la página tras enviar una nueva tarea.
 *
 */

form.addEventListener('submit', (e) => {
    // eliminamos el comportamiento por defecto del boton
    e.preventDefault();

    // Seleccionamos el texto del imput
    const input = form.elements.todo;

    // Creamos una nueva tarea con el texto del imput
    addTodo(input.value);

    // Vaciamos el imput
    input.value = '';

    // Renderizamos la página
    render();
});

/**
 * ##################################################
 * ##  Marcar tarea como realizada / no realizada  ##
 * ##################################################
 *
 * Agrega un evento al ul que marque una tarea como realizada o
 * no realizada.
 *
 * Renderiza la página tras marcar / desmarcar las tareas.
 *
 */

todoList.addEventListener('click', (e) => {
    // Seleccionamos el elemento clickado.
    const { target } = e;

    // Comprobamos que el elemento clikado es un imput de tipo checkbox
    if (target.matches('input[type="checkbox"]')) {
        // Seleccionamos el li más cercano. Valdría con parentElement
        const closestLi = target.closest('li');

        // Obtenemos el index del li.
        const index = closesLi.getAttribute('data-index');

        // Cammbiamos el estado de la tarea.
        toggleTodo(index);

        // Renderizamos la página.
        render();
    }
});

/**
 * #################################################
 * ##  Eliminar tareas marcadas como completadas  ##
 * #################################################
 *
 * Agrega un evento de click al botón que se encarga de limpiar
 * las tareas marcadas como completadas para eliminar estas tareas.
 *
 * Renderiza la página tras eliminar las tareas.
 *
 */

todoClean.addEventListener('click', () => {
    // Limpiamos las teareas marcadas como completadas
    cleanTodos();

    // Renderizamos la página
    render();
});

/**
 * #################################
 * ##  Eliminar TODAS las tareas  ##
 * #################################
 *
 * Agrega un evento de click al botón que se encarga de eliminar
 * todas las tareas para borrar todas las tareas.
 *
 * Renderiza la página tras eliminar las tareas.
 *
 */

todoEmpty.addEventListener('click', () => {
    // Limpiamos las tereas
    if (confirm('¿Estás seguro que quieres elimar todas las tareas?')) {
        deleteAllTodos();

        // Renderizamos la página
        render();
    }
});

/**
 * #####################################
 * ##  Actualizar la lista de tareas  ##
 * #####################################
 *
 * Crea la función render que se encargará de actualizar las tareas
 * en el HTML cada vez que hagamos un cambio.
 *
 */

const render = () => {
    // Vaciamos el contenido del ul con clase 'todo-list'
    todoList.innerHTML = '';

    // Creamos un fragmento de documento
    const frag = document.createDocumentFragment();

    // Recorremos el array
    for (let i = 0; i < State.todos.length; i++) {
        // Creamos el li de la tarea y agregamos un index
        const li = document.createElement('li');
        li.setAttribute('data-index', i);

        // Tarea actual
        const todo = State.todos[i];

        // Agregamos al li la clase done si la tarea esta marcada como hecha
        if (todo.done) li.classList.add('done');

        li.innerHTML = `
            <input type="checkbox" ${todo.done ? 'checked' : ''} />
            <p>${todo.text}</p>
            <time datetime=${todo.date}>${new Date(
            todo.date
        ).toLocaleDateString()}></time>
        `;

        // Agregamos el li al fragmento
        frag.append(li);
    }

    // Agregamos el fragmento al ul
    todoList.append(frag);
};

render();
