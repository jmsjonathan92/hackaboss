// Creamos la función de la fecha
const getCurrentDate = () => new Date().toLocaleDateString('es-ES');

export default getCurrentDate;
