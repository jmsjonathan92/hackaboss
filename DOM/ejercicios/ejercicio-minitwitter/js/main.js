/**
 * Ejemplo de la estructura final:
 *
 *  <ul id="tweets">
 *      <li>
 *          <p>Lo que escriba el usuario en el input.</p>
 *          <footer>
 *              <time>23/3/2021</time>
 *              <button class="action">Borrar</button>
 *          </footer>
 *      </li>
 *  </ul>
 */

'use strict';

// Importo las funciones de helpers
import getCurrentDate from './helpers.js';

// Cualquiera de estas formas son posibles:
const form = document.forms.twitter;
const button = document.querySelector('form > button');
const ul = document.querySelector('ul#tweets');

/**
 * #########################
 * ### INSERTAMOS TWEET ####
 * #########################
 */

button.addEventListener('click', (e) => {
    // Prevenimos el comportamiento por defecto del botón.
    e.preventDefault();

    // Seleccionamos el imput.
    const tweetImput = form.elements.tweet;

    // Almacenamos el valor actual del imput
    const tweetContent = tweetImput.value;

    // Prohibimos que envien tweets vacios
    if (tweetContent === '') {
        alert('No hay caracteres en el tweet');
    } else if (tweetContent.length > 100) {
        alert('La longitud no puede superar los 100 caracteres');
    } else {
        // Creamos el li.
        const li = document.createElement('li');

        // Agregamos el contenido del li:
        li.innerHTML = `
              <p>${tweetContent}</p>
              <footer>
                  <time>${getCurrentDate()}</time>
                  <button class="delete">Borrar</button>
               </footer>
               `;

        // Insertamos el li como primer hijo del ul.
        ul.prepend(li);

        // Vaciamos el contenido del imput
        tweetImput.value = '';
    }
});

/**
 * #########################
 * ### ELIMINAMOS TWEET ####
 * #########################
 */

ul.addEventListener('click', (e) => {
    // Seleccionamos el elemento que hemos pulsado
    const { target } = e;

    // Comprobamos si el elemento pulsado es el botón con clase 'delete'
    if (target.matches('button.delete')) {
        // Seleccionamos el tweet
        const parentLi = target.parentElement.parentElement; // hay que usarlo dos veces porq el botton esta dentro del footer y queremos ir al li

        // Elimino el li
        parentLi.remove();
    }
});
