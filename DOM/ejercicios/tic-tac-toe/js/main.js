'use strict';
import State, { updateState, resetState, checkWinner } from './state.js';
// Seleccionamos el main.
const main = document.querySelector('body > main');
// Seleccionamos el tablero: div con clase "board".
const boardDiv = document.querySelector('main > div.board');
// Seleccionamos las tres lineas de cuadrados: los tres divs dentro del anterior.
const firstRowDiv = document.querySelector('div > div.first-row');
const secondRowDiv = document.querySelector('div > div.second-row');
const thirdRowDiv = document.querySelector('div > div.third-row');
/**
 * #######################
 * ## handleSquareClick ##
 * #######################
 *
 * Esta función manejadora dicta lo que sucede cada vez que un jugador
 * hace click en un cuadrado.
 *
 *  - Si el objetivo es el div con clase "square" comprobamos qué jugador
 *    está jugando: rondas pares "X" rondas impares "O".
 *
 *  - Posteriormente obtenemos el index del cuadrado sobre el que pulsamos.
 *
 *  - Actualizamos el tablero.
 *
 *  - Renderizamos los cambios en el HTML.
 *
 */
const handleSquareClick = (e) => {
    const { target } = e;
    if (target.textContent === '') {
        if (target.matches('div.square')) {
            const actualPlayer = State.round % 2 === 0 ? 'X' : 'O';
            const index = target.getAttribute('data-index');
            updateState(index, actualPlayer);
            render();
        }
    }
};
// Agregamos el evento de click al div con clase "board".
boardDiv.addEventListener('click', handleSquareClick);
/**
 * ######################
 * ## Resetear partida ##
 * ######################
 *
 * Agregamos un manejador de evento al main que compruebe si el elemento
 * clickado es el div con clase "reset". Si es así:
 *
 *  - Eliminamos el elemento padre.
 *
 *  - Reseteamos el tablero.
 *
 *  - Activamos de nuevo la función manejadora "handleSquareClick"
 *
 */
main.addEventListener('click', (e) => {
    const { target } = e;
    if (target.matches('.reset > h2, .reset > p')) {
        target.parentElement.remove();
        resetState();
        render();
        // Activamos de nuevo la función manejadora "handleSquareClick"
        boardDiv.addEventListener('click', handleSquareClick);
    }
});
/**
 * ############
 * ## Render ##
 * ############
 */
function render() {
    // Vaciamos las tres filas de casillas.
    firstRowDiv.textContent = '';
    secondRowDiv.textContent = '';
    thirdRowDiv.textContent = '';
    // Creamos las filas. Para ello debemos recorrer el tablero (board).
    for (let i = 0; i < State.board.length; i++) {
        const div = document.createElement('div'); //Hay que crear el elemento dentro del for porque sino lo va moviendo.
        div.textContent = State.board[i];
        div.classList.add('square');
        div.setAttribute('data-index', i);
        if (i <= 2) {
            firstRowDiv.append(div);
        } else if (i <= 5) {
            secondRowDiv.append(div);
        } else {
            thirdRowDiv.append(div);
        }
    }
    // Creamos un div y le agregamos el contenido de la posición actual
    // del tablero.
    // Agregamos al div la clase "square" y el atributo "data-index" con
    // el valor del index actual.
    // Recuerda que el tablero tiene 9 elementos. Los tres primeros son
    // las casillas de la primera fila, los 3 siguientes las casillas de
    // la segunda fila, y los tres últimos son las casillas de la última
    // fila. Agrega las casillas como hijo de la fila correspondiente.
    // Almacenamos el valor que retorne la función winner.
    const winner = checkWinner();
    console.log(winner);
    // Si hay un ganador...
    if (winner) {
        // Eliminamos el event listener que permite clickar en los divs,
        // es decir, en cada casilla.
        main.removeEventListener('click', handleSquareClick);
        // Creamos un div y le agregamos la clase "reset".
        const resetDiv = document.createElement('div');
        resetDiv.classList.add('reset');
        // Agregamos al div un párrafo con un mensaje que indique el resultado
        // de la partiday un h2 que diga "Try againg".
        resetDiv.innerHTML = `<p>${winner}</p>
            <h2> Try again! </h2>`;
        // Agregamos al main el div.
        main.append(resetDiv);
    }
}
render();
