'use strict';

// esto sirve para exportar funciones o lo que sea, aqui lo guardamos y lo exportamos
// despues lo importamos en el archivo que lo necesitemos

const formatHour = (hours, minutes, seconds) => {
    hours = hours < 10 ? '0' + hours : hours;
    minutes = minutes < 10 ? '0' + minutes : minutes;
    seconds = seconds < 10 ? '0' + seconds : seconds;

    if (seconds % 2 === 0) {
        return `${hours}:${minutes}:${seconds}`;
    } else {
        return `${hours} ${minutes} ${seconds}`;
    }
};

export { formatHour };
