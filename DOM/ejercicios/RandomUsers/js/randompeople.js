/**
 * Cada <li> debería tener una estructura similar a esta:
 *
 * <article>
 *      <header>
 *          <img src="${imagenUsuario}" alt="${nombreCompleto}">
 *          <h1>${nombreCompleto}</h1>
 *      </header>
 *      <p>${ciudad} (${país}), ${añoNacimiento}</p>
 * </article>
 *
 *
 * API: https://randomuser.me/api/?results=10
 */

'use strict';

const n = prompt('¿Cuantos usuarios quieres ver?');
const ul = document.querySelector('.userlist');

async function getCharacter(n) {
    try {
        const data = await (
            await fetch(`https://randomuser.me/api/?results=${n}`)
        ).json();
        ul.innerHTML = '';
        for (let i = 0; i < n; i++) {
            const li = document.createElement('li');
            let imagenUsuario = data.results[i].picture.medium;
            let nombreCompleto =
                data.results[i].name.first + ' ' + data.results[i].name.last;
            let ciudad = data.results[i].location.city;
            let pais = data.results[i].location.country;
            let añoTemporal = data.results[i].dob.date.split('-');
            let añoNacimiento = añoTemporal[0];
            li.innerHTML = `
                    <article>
                        <header>
                            <img src="${imagenUsuario}" alt="${nombreCompleto}">
                            <h1>${nombreCompleto}</h1>
                        </header>
                        <p>${ciudad} (${pais}), ${añoNacimiento}</p>
                    </article>
                    `;
            ul.prepend(li);
        }
    } catch (error) {
        console.log(error);
    }
}

getCharacter(n);
