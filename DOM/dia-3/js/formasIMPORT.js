'use strict';

const sum = (a, b) => a + b;
const sub = (a, b) => a - b;
const mult = (a, b) => a * b;
export const div = (a, b) => a / b; // tambien se podria exportar asi

const PI = 3.1419;

export default PI;

export { sum, sub, mult };
