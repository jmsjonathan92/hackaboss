'use strict'

async function characters(month) {
    try {
        let url = 'https://rickandmortyapi.com/api/character';
        const resultados = [];

        while (url !== null) {
            const response = await fetch(url);
            const { results, info } = await response.json();

            for (let result of results) {
                let dia = new Date(result.created);
                if (month === dia.getMonth() + 1) resultados.push(result);
            }
            url = info.next;
        }
        console.log(resultados);
    } catch (error) {
        console.log(error);
    }
} 

characters(1);