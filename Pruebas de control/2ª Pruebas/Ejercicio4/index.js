'use strict'
/* Escribe una función que, al recibir un array como parámetro, elimine los strings repetidos del mismo.
    No se permite hacer uso de Set ni Array.from(). */

const names = [
    'A-Jay',
    'Manuel',
    'Manuel',
    'Eddie',
    'A-Jay',
    'Su',
    'Reean',
    'Manuel',
    'A-Jay',
    'Zacharie',
    'Zacharie',
    'Tyra',
    'Rishi',
    'Arun',
    'Kenton',
];

function eliminarDuplicados (array) {
    let arrrySinDuplicados = [];
    
    for (let i = 0; i < array.length; i++) {
        if (!arrrySinDuplicados.includes(array[i])){
            arrrySinDuplicados.push(array[i])
        } 
    }
    return arrrySinDuplicados;
}
console.log(eliminarDuplicados(names));
