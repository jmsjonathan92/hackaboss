'use strict'

let personRandom = async (URL, usuarios) => {
    try {
       
        let personasArry = [];
   
        for (let i = 0; i < usuarios; i++) {

            let response = await fetch(URL)
            let personaJson = await response.json()
            
            let nombre = personaJson.results.map((nombre) => nombre.name)
            let genero = personaJson.results.map((genero) => genero.gender)
            let pais = personaJson.results.map((pais) => pais.location.state)
            let email = personaJson.results.map((emailAddress) => emailAddress.email)
            let foto = personaJson.results.map((foto) => foto.picture.medium);
            let personaArray = [nombre, genero, pais, email, foto];

            personasArry.push(personaArray);
        }
        console.log(personasArry);
    } catch (error) {
        console.log(error);
    }
};


personRandom('https://randomuser.me/api', 5);