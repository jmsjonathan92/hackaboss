'use strict'

let horas = 0;
let minutos = 0;
let segundos = 0;

setInterval(() => {
    let dias = 0;
    segundos += 5;

    if (segundos >= 60) {
        minutos ++;
        segundos = 0;
    } else if (minutos >= 60) {
        horas ++;
        minutos = 0;
    } else if (horas >= 24) {
        dias ++;
        horas = 0;
    }

    let hora_2 = horas < 10 ? '0' + horas : horas;
    let min_2 = minutos < 10 ? '0' + minutos : minutos;
    let sec_2 = segundos < 10 ? '0' + segundos : segundos;

    console.log(`${dias} dia, ${hora_2}:${min_2}:${sec_2}`);
}, 5000)


