'use strict'

const num = prompt('Escribe un número');
const base = prompt('Binario 2, decimal 10');
let binario = 2;
let decimal = 10;

const convertidor = (num, base) => {
    let resultado = 0;

    if (base === binario) {
        let numDecimales = num.toString().split('');
        let p = (numDecimales.length) - 1;
        for (const numDecimal of numDecimales)  {
            resultado += numDecimal *(2 ** p);
            p--;
        }
    }else {
        let cociente = num;
        let binNumeros = [];
        do {
            binNumeros.push(cociente % 2);
            cociente = Math.floor(cociente / 2);
        } while (cociente > 0);
        resultado = binNumeros.reverse().join('');
    }
    return resultado;
}

console.log(convertidor(num, base));