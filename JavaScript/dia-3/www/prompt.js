'use strict';

/* con el promp le envias un mensaje o le pides un dato al cliente o le envias un mensaje
emergente */

alert('Soy una alerta, hasta que le des al ok no podrás entrar');

console.log('Al dar ok podrás ver este mensaje'); // con esto podremos hacer que acepte o no el usuario

/* confirm('estás de acuerdo? '); */

/* con el if podemos poner que pasaria si rechaza o no */

if (confirm('Estas de acuerdo?')) {
    console.log('Aceptaste los terminos');
} else {
    // podriamos borrar esto por que asi solo enseñariamos algo si lo acepta
    console.log('Rechazaste los terminos');
}

if (confirm('aceptas los terminos?')) alert('estas de acuerdo');

const num_A = Number(prompt('Dime el primer nº'));

console.log(num_A);

console.log(typeof num_A);

const name = prompt('Dime tu nombre');
const age = prompt('Dime tu edad');
alert(`Hola, ${name} Tienes ${age} años`);
