'use strict';

const num_A = 4;
const num_b = 0;
const option = 0;

switch (option) {
    case 0:
        console.log(num_A + num_B);
        break;

    case 1:
        console.log(num_A - num_B);
        break;

    case 2:
        console.log(num_A * num_B);
        break;

    case 3:
        console.log(num_A / num_B);
        break;

    case 4:
        console.log(num_A ** num_B);
        break;

    default:
        throw new Error('Tipo de operación incorrecto.');
}
