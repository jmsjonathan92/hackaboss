'use strict0';

/* el bucle 'for' necesita 3 condiciones y en este orden y separados siempre por (;):
    1- Contador. Declaramos un contador con un let y le asignamos un valor
    2- Decisión. Mientras la condición se repita el bucle sigue dando vueltas
    3. Incremento/ decremento en el contador.
*/
for (let contador = 10; contador > 0; contador -= 2) {
    console.log(contador);
}
