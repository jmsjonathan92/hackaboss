'use strict';

/* al poner los numeros entre comillas lo pondra como un sting entonces no hara la suma
y lo aumentara al numero y convertira todo en un sting, la solucion podriamos convertir B a number
y despues hay otras dos formas que las ponemos como ejemplo */

let A = 12;
let B = '2';

B = parseInt(B); //esto es un metodo, podriamos poner el parseInt directaente en la console, pero solo funcionaria con numeros enteros
B = parseFloat(B); // con este seria para numeros con decimales también
B = Number(B); // este valdria para todo

console.log(A + B);

let text_A = 'Hola, ';
let text_B = 'me llamo Jony y';
let text_C = 'tengo 29 años.';

console.log(text_A + text_B + '' + text_C);

console.log(`${text_A} me llamo Jony y ${text_C}`); // con esta forma de las comillas al reves podemos ahorrarnos poner los espacios como arriba

for (let i = 0; i < 10; i++) {
    console.log(`Repetición nº 
    
    ${i + 1}`); // con esto se respeta los saltos de linea.
}
