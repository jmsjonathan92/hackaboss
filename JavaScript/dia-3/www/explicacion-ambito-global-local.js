'use strict';
/* si tenemos algo que esta fuera de un bloque de llaves como el const name esta definido 
en un ambito global si estuviera entre parentesis seria un ambito local, en los globales podemos 
acceder siempre en los locales solo dentro de ese mismo local */

/* Podemos entrar dentro de los bloques a los bloques de arriba sin parar el primer bloque
pero no podremos pasar del primer bloque al segundo */
const name = 'Carlos';

{
    let age = 17;
    console.log(age);
    console.log(number); //esto no se puede hacer !!!

    {
        let number = 7;
        console.log(number);
        console.log(age); // pero esto si se puede hacer !!
    }

    console.log(name); // a este también por que esta en ambito global!
}

{
    console.log(number); // aquí no podría por que esta en otra casilla, en este ej solo podria ir al global
}
