'use strict';

/*while se podria utilizar como for, aquí un ejemplo 
Aquí hay que darle valor al let counter y abajo con counter indicamos como 
queramos que funcione, ++ suma uno o += 2 por ejemplo etc...*/

let counter = 0;

while (counter < 10) {
    console.log(counter);
    counter++;
}

// el do valdría para que muestre algo hasta que se cumpla la condición

let counter_B = 10;

do {
    console.log('Hola');
} while (counter_B < 8);

console.log('##########');

let contador = 0;

while (contador <= 100) {
    console.log(contador);
    contador += 10;
}

console.log('#############');

let contador_2 = 200;

while (contador_2 >= 0) {
    console.log(contador_2);
    contador_2 -= 25;
}
