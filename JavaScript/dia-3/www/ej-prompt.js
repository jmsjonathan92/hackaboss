'use strict';

/*  para que salga el prompt debemos poner el confirm dentro del if si no crear una 
const con el confirma, pero mejor asi */

const name = prompt('Como es tu nombre y tu primer apellido?');

if (confirm(`${name}, le apetece un cafe?`)) {
    // las comillas al reves se utilizan para utilizar las operaciones o asi dentro del texto
    alert(`Bienvenido ${name} En breves recibiras su café`);
} else {
    alert(`Bienvenido ${name} no quieres cafe`);
}
