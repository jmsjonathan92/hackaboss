/* CODIGO ASICRONO */

// APIs

// setTimeout sirve para que tarde mas en salir ejemplo:

const myTime = setTimeout(() => {
    console.log('Hoy va a ser un buen día');
}, 5000);

console.log('veremos');

clearTimeout(myTime); // con esto no se ejecutará

//serInterval es para que lo repita seguido

const myInterval = setInterval(() => console.log('ALARMA'), 1000);

setTimeout(() => clearInterval(myInterval), 10000); // con esto
// le das una duración de 10s en este ejemplo, tambien se podria borrar como lo anterior
