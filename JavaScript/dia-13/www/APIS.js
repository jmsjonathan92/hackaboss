// APIS
/* La información que nos interesa esta en el body pero esta en formato JSON y no la muestra,
para eso tenemos que hacer esto */
/* 
fetch('https://rickandmortyapi.com/api/character')
    .then((response) => response.json())
    .then((characters) => {
        console.log(characters);
    })
    .catch((error) => console.log(error)); */

// funciones asincronas, asi es otra forma de trabajar con ellos

async function getCharacters() {
    const response = await fetch('https://rickandmortyapi.com/api/character');

    const data = await response.json();
    console.log(data);
}
getCharacters();
