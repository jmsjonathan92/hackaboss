/**
 * ESTO SIRVE PARA ELIMINAR LAS VECES QUE HACEMOS MUCHOS CALLBACKS
 * ESTO PUEDE ACEPTARSE O RECHAZARSE
 * ESTE EJEMPLO NO ES MUY HABITUAL, ES SOLO UN EJEMPLO
 *  */

'use strict';

const myPromise = new Promise((resolve, reject) => {
    const random = Math.floor(Math.random() * 9);

    setTimeout(() => {
        if (random <= 5) {
            resolve('Todo ha ido bien');
        } else {
            reject('Todo mal');
        }
    }, 2000);
});

myPromise
    .then((data) => console.log(data))
    .catch((error) => console.error(error))
    .finally(() => console.log('Me ejecuto sí o sí'));

/* Aquí hemos creado un número aleatorio y si sale un numero menor de 5 ha ido todo bien
y si sale mal pues todo ha ido mal. */
