/* #################
 * ## Ejercicio 3 ##
 * #################
 *
 * Listar todos los municipios de la provincia de Lugo.
 * Además, el nombre de los municipios debe de estar ordenado por orden
 * alfabético inverso.
 *
 * - Resvolverlo con async / await
 *
 * API: https://www.el-tiempo.net/api
 *
 */
'use strict';

fetch('https://www.el-tiempo.net/api/json/v2/municipios')
    .then((response) => response.json())
    .then((municipios) => {
        const municipiosLugo = municipios
            .filter((municipio) => municipio.NOMBRE_PROVINCIA === 'Lugo')
            .map((municipio) => municipio.NOMBRE);
        console.log(municipiosLugo.sort().reverse());
    })
    .catch((error) => console.log(error));

// otra forma de hacerlo es con async

const getMunicipalities = async (URL) => {
    try {
        const response = await fetch(URL);

        const municipios = await response.json();

        const municipiosLugo = municipios
            .filter((municipio) => municipio.NOMBRE_PROVINCIA === 'Lugo')
            .map((municipio) => municipio.NOMBRE);

        console.log(municipiosLugo.sort().reverse());
    } catch (error) {
        console.log(error);
    }
};
getMunicipalities('https://www.el-tiempo.net/api/json/v2/municipios');
