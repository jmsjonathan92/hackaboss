/* #################
* ## Ejercicio 2 ##
* #################
*
* Listar todas las provincias del estado. 
+
* API: https://www.el-tiempo.net/api 
*
*/

const provincias = fetch('https://www.el-tiempo.net/api/json/v2/provincias')
    .then((response) => response.json())
    .then((data) => {
        for (let i = 0; i < data.provincias.length; i++) {
            const provincias = data.provincias[i];
            console.log(provincias.NOMBRE_PROVINCIA);
        }
    })
    .catch((error) => console.log(error));
