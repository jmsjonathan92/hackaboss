/* #################
 * ## Ejercicio 4 ##
 * #################
 *
 * Escribe un programa que permita al usuario introducir elementos en un array.
 * El programa finalizará cuando el usuario introduzca el string "fin", y se
 * mostrará por consola el contenido del array.
 *
 */

'use strict';

// Array en el que iremos pusheando elementos.

const result = [];

let userContent;

do {
    // aqui le permitimos al usuario intrudocir un elemento.
    userContent = prompt('Introduce lo que sea');

    // para no mostrar la palabra fin dentro se haria asi
    if (userContent !== 'fin') {
        // ahora deberemos de pushearlo al arraid (result)
        result.push(userContent);
    }
} while (userContent !== 'fin');

// ahora mostramos el contenido en la consola

console.log(result);
