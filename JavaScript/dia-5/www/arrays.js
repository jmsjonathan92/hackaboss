'use strict';

// aqui vamos a crear cajones arrays, cajones que podemos guardar muchas cosas en el
/* las posiciones son consecutivas desde 0 */
const myArray = ['Amarrilo', false, 455, 13, 25, 54];
// const colors = Array('blanco', true, 874, 'Jony');
/* 
        console.log(myArray);
        console.log(colors);
 */
// para ir a una posicion fija se hace asi:

// console.log(myArray[2], myArray[0]);

// para saber la longitud de los elementos podemos utilizar:
// console.log(myArray.length);

// como es una constante no podriamos modificarlo pero con este bucle for podriamos:

for (let i = 0; i < myArray.length; i++) {
    console.log(myArray);
}
