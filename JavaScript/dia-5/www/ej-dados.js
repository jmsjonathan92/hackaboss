/* #################
 * ## Ejercicio 4 ##
 * #################
 *
 * Crea un programa que simule las tiradas de un dado:
 *
 *      - El dado debe generar en cada tirada un valor entre 1 y 6 (incluídos).
 *
 *      - Cuando el total de tiradas supere o iguale los 100 puntos muestra
 *        un mensaje indicando que se han alcanzado los 100 puntos.
 *
 */

'use strict';

// TODO 1 crear un bucle que genere un numero aleatorio entero.

// TODO 2. Mostramos la tirada de los dados con su valor

// TODO 3. En cada tirada acumular los valores que salgan

// TODO 4. Sumamos los valores, cuando salga 100 o más puntos finaliza el juego.

// 1:

const dado = () => Math.ceil(Math.random() * 6); // Con esto siempre será al alza, nunca saldrá un 0.
let total = 0;
// 2:

/* Esto podría hacerse asi:
    for (let total = 0; total < 100; total += dado()) {
    console.log(total);
} 
o tambien:
*/

for (let i = 1; total < 100; i++) {
    const numero = dado(); //aqui guardamos el numero que salio en la constante
    total += numero; // y aqui lo sumamos al total de numeros anteriores

    console.log(
        `Tirada ${i}, ha salido un ${numero}. Tienes un total de ${total} puntos.`
    );
    console.log('##############################');
}

console.log('Fin de la partida');
