'use strict';

function sayHello() {
    return 'Hola';
}

const result_A = sayHello();

const result_B = sayHello; // aqui nos falta () para llamar a la función, entonces nos muestra todo en la consola

console.log(result_B);
console.log(result_A);

/* Una función podemos llamarla cuando queramos  pero una expresion de funcion
que sería:
 */
const sayBye = function () {
    return 'adios';
};

//Pero si la llamamos asi solo podremos llamarla despues de haberla creado  */*/

// ARROW FUNCTION - otro tipo de función

const sayNothing = () => {
    return 'Que te vaya bien.';
};

console.log(sayNothing());
