/* #################
 * ## Ejercicio 1 ##
 * #################
 *
 * Crea una función "showInfo()" a la que le pases como parámetros un nombre y una edad:
 *
 *      - La función deve devolver un string del tipo: "Eres Pablo y tienes 33 años". Recuerda
 *        que el nombre y la edad son parámetros, pueden variar en cada llamado.
 *
 *      - Muestra lo que devuelva la función por consola. Aplica lo aprendido sobre "template
 *        strings" (template literals).
 *
 *      - Llama a la función tres veces pasándole distintos valores.
 */
/* 
const showInfo = (name, age) => {
    return `Eres ${name} y tienes ${age} años.`;
};

console.log(showInfo('Juan', 54));
console.log(showInfo('Luis', 44));
console.log(showInfo('María', 32));

// tambien valdria

const showInfo = (name, age) => `Eres ${name} y tienes ${age} años.`;

console.log(showInfo('Juan', 54));
console.log(showInfo('Luis', 44));
console.log(showInfo('María', 32)); */

// esto seria con prompt

showInfo();
function showInfo(nombre, edad) {
    nombre = prompt('Escribe tu nombre');
    edad = prompt('Escribe tu edad');
    console.log(`Tu nombre es: ${nombre} y tienes: ${edad} años.`);
}
