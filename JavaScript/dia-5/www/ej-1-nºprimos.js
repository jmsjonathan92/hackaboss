'use strict';

/* Vamos a crear una funcion que nos diga si un numero es primo o no */
/* 
function isPrime(num) {
    for (let i = 2; i < num; i++) {
        if (num % i === 0) return false;
    }

    return true;
}

console.log(isPrime(56));

const result = isPrime(7);

console.log(result);
*/

// esta es otra forma de hacerlo
function primosTodos(numero) {
    alert((numero = Number(prompt('Introduce un numero'))));
    for (let i = 2; i < numero; i++) {
        if (numero % i === 0) {
            return false;
        }
    }
    return numero !== 1;
}
console.log(primosTodos());

/* 2º parte crear lo mismo pero la consola nos muestra los nº primos del 2 al nº que nosotos
le indiquemos */
/* 
function isPrime(num) {
    for (let i = 2; i <= num / 2; i++) {
        if (num % i === 0) return false;
    }

    return true;
}

function allPrimes(num) {
    for (let i = 2; i <= num; i++) {
        if (isPrime(i)) console.log(`El nº ${i} es primo.`);
    }
}

allPrimes(20);

 */
