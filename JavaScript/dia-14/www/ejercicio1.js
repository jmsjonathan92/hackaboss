/**
 * ###############################################################
 * ## Obtener un array con las series de las 5 primeras páginas ##
 * ###############################################################
 *
 * API: https://www.episodate.com/api
 *
 */

'use strict';

const getSeries = async () => {
    try {
        const page = 5;
        const series = [];
        for (let i = 1; i <= page; i++) {
            const response = await fetch(
                'https://www.episodate.com/api/most-popular?page={i}'
            );
            const data = await response.json();
            series.push(...data.tv_shows);
        }
        return series;
    } catch (error) {
        console.log(error);
    }
};

getSeries()
    .then((data) => console.log(data))
    .catch((error) => console.log(error));
