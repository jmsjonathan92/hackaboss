/**
 * #################
 * ## Ejercicio 2 ##
 * #################
 *
 * Obtener un array con los todos personajes. Debe existir la
 * posibilidad de filtrar por "status" y por "species".
 *
 * ¡No utilizar el método filter!
 *
 * API: https://rickandmortyapi.com/
 *
 */

'use strict';

async function getCharacters(status, species) {
    const pages = (
        await (await fetch(`https://rickandmortyapi.com/api/character`)).json()
    ).info.pages;
    let charactersArray = [];
    for (let index = 1; index <= 2; index++) {
        const characters = (
            await (
                await fetch(
                    `https://rickandmortyapi.com/api/character?page=${index}`
                )
            ).json()
        ).results;
        let filteredCharacter = characters.map((character) => {
            let objeto = {
                name: character.name,
                specie: character.species,
                status: character.status,
            };
            return objeto;
        });
        charactersArray = charactersArray.concat(filteredCharacter);
    }
}
getCharacters('alive', 'Human');
