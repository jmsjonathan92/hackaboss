'use strict';

/*  Vamos a ver como declarar las funciones, las funciones
se pueden utilizar en cualquier parte del documento, una vez creadas */

/* toda funcion empieza con function y un verbo, en este ejemplo queremos
que sume o reste entonces calculate, abrimos parentesis y metemos los parametros dentro */
/* 
function name(name = 'guapo') {
    // si queremos que ponga algo por defecto por si el usuario no pone nada pondremos name = 'Jose'
    return `calcula ${name}`;
} // asi seria una funcion, no aparecera nada en la consola hasta que la llamemos

// la llamamos asi y podremos llamarla las veces que queramos:
console.log(name('Jony'));
const myName = name();

console.log(myName); */

/** esto sirve para documentar la función, la documentamos y pasando el ratón por encima nos indica como es
 *
 * @param {number} a Primer número
 * @param {number} b Segundo número
 * @param {String} option Tipo de operación (+ o -)
 *  */

function calculate(a, b, option) {
    if (option === '+') {
        return a + b;
    } else if (option === '-') {
        return a - b;
    } else {
        throw new Error('No se reconoce el tipo de operación');
    }
}

console.log(calculate(2, 2, '+'));
console.log(calculate(2, 6, '*'));
console.log(calculate(2, 2, '-'));
