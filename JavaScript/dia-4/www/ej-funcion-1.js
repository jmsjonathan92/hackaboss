'use strict';

/*  pasar el ejercicio del dia 2 de la calculadora pasarlo a funcion  */

const myNumber = 5;

function calculate(a, b, option) {
    if (option === '+') {
        return a + b;
    } else if (option === '-') {
        return a - b;
    } else if (option === '*') {
        return a * b;
    } else {
        throw new Error('No se reconoce el tipo de operación');
    }
}

console.log(calculate(8, 2, '+'));
console.log(calculate(8, myNumber, '*'));
