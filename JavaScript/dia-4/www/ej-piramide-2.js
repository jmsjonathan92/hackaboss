'use strict';

/*  Crear una piramide que tenga forma de:
1
22
333
etc... y con numeros por fila*/

const height = 7;
let lineContent = '';

for (let lines = 0; lines < height; lines++) {
    for (let nums = 1; nums <= lines + 1; nums++) {
        lineContent += lines + 1;
    }

    console.log(lineContent);
    lineContent = ''; // esto hay que ponerlo para que se organicen las lineas.
}
