'use strict';

/* utilizar el bucle for para crear una figura de astericos 
hacia la derecha, debe poderse modificar la altura de la piramide  */

const height = 5;

for (let lines = 0; lines < height; lines++) {
    let lineContent = '';
    // espacios:
    for (let spaces = height - lines - 1; spaces > 0; spaces--) {
        lineContent += ' ';
    }

    // asteriscos:
    for (let asterik = lines + 1; asterik > 0; asterik--) {
        lineContent += '*';
    }

    console.log(lineContent);
}
