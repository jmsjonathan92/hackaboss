'use strict';

/*  pasar el ejercicio del dia 3 de la bomba pasarlo a funcion  */
/**
 * @param {Contraseña} bombPassword Para generar los números aleatorios.
 * @param {limite} limit Intentos que el usuario tiene
 *  *  */

function numberRandom() {
    return Math.ceil(Math.random() * 10);
}

function bombDesactivate(limit, bombPassword) {
    for (let i = limit; i > 0; i--) {
        const userNumber = Number(prompt(`Intento nº ${i}. Indica un nº: `));

        if (userNumber === bombPassword) return true;
    }
    return false;
}

console.log(bombDesactivate(10, numberRandom()));
