'use strict';

// en esta piramide haremos lo mismo que la anterior pero empezando desde 1 cada fila

/* const height = 7;
let lineContent = '';

for (let lines = 0; lines < height; lines++) {
    let lineContent = '';

    for (let nums = 0; nums <= lines; nums++) {
        lineContent += nums + 1;
    }

    console.log(lineContent);
} */

// tambien podremos hacerlo asi:

const height = 5;
let lineContent2 = '';

for (let lines = 0; lines < height; lines++) {
    lineContent2 += lines + 1;
    console.log(lineContent2);
}
