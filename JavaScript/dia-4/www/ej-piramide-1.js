'use strict';

/* vamos a crear una piramide con emojis utilizando el bucle for  */

const height = 5;
let fila = '';

for (let i = 0; i < height; i++) {
    fila += '😎';
    console.log(fila);
}
