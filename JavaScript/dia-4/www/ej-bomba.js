'use strict';

/*  Aquí vamos a crear un ejercicio emergente donde nos pida un numero al azar
y tengamos 5 intentos, y si acertamos el numero desbloqueamos la bomba y si no 
explotara la bomba */

/* const randomNumber = Math.random() * 10; // con esto creamos un numero aleatorio entre los 10 primeros numeros. Nunca va llegar a diez en este ejemplo.

for (let i = 0; i < 10; i++) {
    console.log(Math.random() * 10); // con esto creamos un numero aleatorio pero con decimales
} */

/*  con esta funcion redondeamos los numeros y salgan enteros.
    - Math.round(numero) - con esto redondeamos a lo mas cercano
    - Math.ceil(numero)  - con este redondeamos al alza
    - Math.floor(numero) - con esto redondeamos a la baja
    */

/* let randomNumber = Math.ceil(Math.random() * 10);
 */
// Objetivos del ejercicio:

// 1. Generar un nº entero aleatorio del 1 al 10 (la contraseña)
let randomNumber = Math.ceil(Math.random() * 10);

// 2. Damos al usuario 5 intentos para poner la contraseña
// en un bucle for se puede repetir una const por que es como si se empezara de 0
for (let i = 5; i > 0; i--);
{
    // 3. permitimos al usuario insertar un nº
    const userNumber = Number(prompt(`Intento nº ${i}. Indica un nº: `));

    // 4. COmprobamos si el nº puesto es igual a la contraseña
    if (userNumber === password) {
        // 5. Si acerto finalizamos el buclque y mostramos un mensaje de que se desactivo
        alert('Se ha desactivado la bomba');
        break;
    } else if (i === 1) {
        // 7 si no coincide y no hay mas intentos mostramos mensaje de que exploto la bomba
        alert('BOM! Exploto la bomba');
    }
    // 8. De lo contrario seguimos intentadolo hasta que acierte o falle
}
