/* ################
 * ## Pirámide 6 ##
 * ################
 *
 * Utiliza el bucle "for" para crear las siguiente figura con asteriscos (*). Debe existir una
 * variable que permita modificar la altura de la pirámide. Para el ejemplo expuesto a
 * continuación se ha tomado como referencia una altura de 4:
 *
 * - Figura:
 *
 *        *
 *       ***
 *      *****
 *     *******
 *      *****
 *       ***
 *        *
 */

'use strict';

const height = 4;

// piramide arriba
for (let lines = 0; lines < height; lines++) {
    let lineContent = '';
    // espacios:
    for (let spaces = height - 1 - lines; spaces > 0; spaces--) {
        lineContent += ' ';
    }

    // asteriscos:
    for (let asterik = 2 * lines + 1; asterik > 0; asterik--) {
        lineContent += '😎';
    }

    console.log(lineContent);
}

// piramide abajo
for (let lines = height - 1; lines > 0; lines--) {
    let lineContent = '';
    // espacios:
    for (let spaces = height - lines; spaces > 0; spaces--) {
        lineContent += ' ';
    }

    // asteriscos:
    for (let asterik = 2 * lines + 1; asterik > 0; asterik--) {
        lineContent += '😎';
    }

    console.log(lineContent);
}
