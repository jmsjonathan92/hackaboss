'use strict';

/* Esto es un objeto, y podría ir dentro muchas mas constantes, como aqui vemos el nombre de la persona
su edad y su animal, dentro con el nombre de su animal y su tipo de animal y dentro podriamos
meter mas datos, todo con , y {} divididos */
const Person = {
    name: 'Carlos',
    age: 29,
    favouriteFood: ['Hamburguesa', 'pizza'],
    pet: {
        name: 'Carbón',
        type: 'Gato',
        otraPropiedad: {
            lugar: 'Galicia',
        },
    },
    sayHello: function () {
        console.log(`Soy ${Person.name} y tengo ${Person.age}`);
    },
};

Person.age = 45; // se puede cambiar cualquier valor !!

console.log(Person.age); // para ir directamente al sitio que queramos que nos muestre directamente
console.log(Person);

Person.sayHello();

//para hacer una copia de Person pero sin ser la misma memoria seria:

const copiaPerson = {
    ...Person,
};

copiaPerson.favouriteNumber = 5;

console.log(copiaPerson);
