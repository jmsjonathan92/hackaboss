'use strict';

/* EJERCICIO 1

    - Crea el obejo coche y asígnale las propiedas modelo, marca y color-
    
    - Muestra el obejo por medio de un console.log
    
    - Modifica el valor de la propiedad color y agrega la propiedad año de matriculación.
    
    - Utiliza un confirm para mostar por consola las propiedas, o los valors.
      Si la persona acepta el confirm se mostrarán las propiedas, de lo contrario, se
      mostrarán los valores.*/

const coche = {
    modelo: 'Focus',
    marca: 'Ford',
    color: 'Azul',
};

coche.color = 'rojo';
coche.añoDeMatriculacion = 1999;

console.table(coche);

if (confirm('Para ver las propiedas tienes que aceptar')) {
    console.log(Object.keys(coche));
} else {
    console.log(Object.values(coche));
}
