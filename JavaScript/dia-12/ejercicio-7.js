/**
 * #################
 * ## Ejercicio 7 ##
 * #################
 *
 * Crea un nuevo array con los amigos en común que tienen dos usuarios de Instagram. El ejercicio
 * debe ser resuelto sin utilizar ningún método excepto push.
 *
 */

'use strict';

const pacosFollowers = ['Manolo', 'Marta', 'Pablo', 'Kevin'];
const luciasFollowers = ['Pablo', 'Marta', 'Ana'];
const comunes = [];

/* 
for (const follower of pacosFollowers) {
    if (luciasFollowers.indexOf(follower) !== -1) {
        console.log(`${follower} es un amigo común.`);
    }
}
 */

for (const paco of pacosFollowers) {
    for (const lucia of luciasFollowers) {
        if (paco === lucia) {
            comunes.push(paco);
            console.log(`${lucia} es un amigo común.`);
        }
    }
}

console.log(comunes);
