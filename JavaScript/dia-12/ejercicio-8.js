/**
 * #################
 * ## Ejercicio 8 ##
 * #################
 *
 * Dado el siguiente tablero (un array de arrays) haz las modificaciones necesarias para
 * lograr esto:
 *
 *     - Figura 1:
 *
 *      [
 *          ['X', '-', '-', '-', 'X'],
 *          ['-', 'X', '-', 'X', '-'],
 *          ['-', '-', 'X', '-', '-'],
 *          ['-', 'X', '-', 'X', '-'],
 *          ['X', '-', '-', '-', 'X']
 *      ];
 *
 *
 *     - Figura 2:
 *
 *      [
 *          ['X', 'X', 'X', 'X', 'X'],
 *          ['X', '-', '-', '-', 'X'],
 *          ['X', '-', '-', '-', 'X'],
 *          ['X', '-', '-', '-', 'X'],
 *          ['X', 'X', 'X', 'X', 'X']
 *      ];
 *
 */

'use strict';

const board = [
    ['-', '-', '-', '-', '-'],
    ['-', '-', '-', '-', '-'],
    ['-', '-', '-', '-', '-'],
    ['-', '-', '-', '-', '-'],
    ['-', '-', '-', '-', '-'],
];

let counter = board.length - 1;

for (let i = 0; i < board.length; i++) {
    board[i][i] = 'X';
    board[i][counter] = 'X';
    counter--;
}

console.log(board);

const board2 = [
    ['-', '-', '-', '-', '-'],
    ['-', '-', '-', '-', '-'],
    ['-', '-', '-', '-', '-'],
    ['-', '-', '-', '-', '-'],
    ['-', '-', '-', '-', '-'],
];

for (let j = 0; j < board2.length; j++) {
    if (j === 0 || j === board2.length - 1) {
        board2[j] = ['X', 'X', 'X', 'X', 'X'];
    } else {
        board2[j][0] = 'X';
        board2[j][board2.length - 1] = 'X';
    }
}
console.log(board2);
