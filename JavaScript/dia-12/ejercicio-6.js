/**
 * #################
 * ## Ejercicio 6 ##
 * #################
 *
 * Dada la función "simpleMode(numsArray)" tome el aprámetro numsArray y devuelva el número
 * que aparece con más frecuencia.
 *
 * Por ejemplo: si numsArray contiene [10, 4, 5, 2, 4] la salida debería ser 4. Si hay existen
 * dos números que se repiten el mismo número de veces devuelve el que apareció primero en el
 * array (por ejemplo [5, 10, 10, 6, 5] debería devolver 5 porque apareció primero).
 *
 * Si no hay ningún modo, devuelve -1. El array no estará vacío.
 *
 */

'use strict';
const nums = [10, 4, 5, 2, 4];
const rept = [0, 0, 0, 0, 0];

function simpleMode(numsArray) {
    for (let i = 0; i < numsArray.length; i++) {
        for (let j = 0; j < numsArray.length; j++) {
            if (numsArray[i] === numsArray[j]) rept[i] += 1;
        }
    }
    console.log(rept); // aqui imprimimos las veces que se repite no la const

    let maximo = 0;
    let indice = 0;

    for (let i = 0; i < rept.length; i++) {
        if (rept[i] > maximo) {
            indice = i;
            maximo = rept[i];
        }
    }

    console.log(
        `El número que más se repite es el ${nums[indice]} y se repite ${maximo}`
    );
}

simpleMode(nums);
