/**
 * #################
 * ## Ejercicio 3 ##
 * #################
 *
 * Dada la función "letterCapitalize(string)" tome el parámetro string que se le pasa y ponga
 * en mayúscula la primera letra de cada palabra. Las palabras estarán separadas por un solo espacio.
 *
 */

'use strict';

const text = 'camióm es muy pequeño';

function palabraCapitalize(string) {
    let palabraSalida = '';
    for (let i = 0; i < string.length; i++) {
        if (i === 0) palabraSalida = palabraSalida + string[i].toUpperCase();
        else palabraSalida = palabraSalida + string[i];
    }
    return palabraSalida;
}

function letterCapitalize(string) {
    const palabras = string.split(' ');
    let resultado = '';
    for (const palabra of palabras) {
        resultado = resultado + palabraCapitalize(palabra) + ' ';
    }
    return resultado;
}

console.log(letterCapitalize(text));
