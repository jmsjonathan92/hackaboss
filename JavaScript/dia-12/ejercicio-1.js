/**
 * #################
 * ## Ejercicio 1 ##
 * #################
 *
 * Dada la función "longestWord(wordsArray)" tome el parámetro wordsArray y devuelve la palabra
 * más larga del array.
 *
 * Si hay dos o más palabras que tienen la misma longitud, devuelve la primera palabra.
 *
 */

'use strict';

const palabras = [
    'caramelo',
    'palabra',
    'superfragilistica',
    'superfragilistico',
    'cascanueces',
    'sobrenatural',
];

function longestWord(wordsArray) {
    let longitud = 0;
    let candidata = '';
    for (const palabra of wordsArray) {
        if (palabra.length > longitud) {
            longitud = palabra.length;
            candidata = palabra;
        }
    }
    return candidata;
}

console.log(longestWord(palabras));
