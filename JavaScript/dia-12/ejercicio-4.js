/**
 * #################
 * ## Ejercicio 4 ##
 * #################
 *
 * Dada la función "timeConvert(minutes)" toma el parámetro minutes y devuelve el número
 * de horas y minutos a los que el parámetro se convierte (es decir, si minutes = 63 entonces
 * la salida debería ser 1:03). Separe el número de horas y minutos con dos puntos.
 *
 */

'use strict';

function timeConvert(minutes) {
    const horas = Math.floor(minutes / 60);
    const minutos = minutes % 60;
    let resultado = '';
    if (minutos >= 9) {
        resultado = horas + ':' + minutos;
    } else resultado = horas + ':0' + minutos;
    return resultado;
}

console.log(timeConvert(62));
