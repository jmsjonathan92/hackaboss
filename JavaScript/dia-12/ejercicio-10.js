/*
 * ####################
 * ##  Ejercicio 10  ##
 * ####################
 *
 * 1. Crea el objeto "fox" y el objeto "chicken" a partir de la función "newAnimal". Ambos objetos
 *    tendrán la propiedad "type", que será "zorro" o "gallina", y el método "breed". Este método
 *    devuelve un nuevo zorro o una nueva gallina (en función del animal).
 *
 * 2. A mayores, el zorro tiene el método "goLunch" que recibe un array de animales como parámetro,
 *    busca una gallina en el array y la elimina.
 *
 * 3. Crea una función que devuelva un nuevo array con dos animales al azar. A la función debemos
 *    pasarle un objeto zorro y un objeto gallina como argumentos. Este array debe retornar un array
 *    con una de las siguientes combinaciones:
 *
 *   - 2 zorros.
 *
 *   - 2 gallinas.
 *
 *   - 1 gallina / 1 zorro.
 *
 *
 * 4. Crea otra función que reciba un array con dos animales y haga lo siguiente:
 *
 *   - Si en el array hay 2 animales del mismo tipo utilizamos la función breed para incorporar un tercer
 *     animal en el array, retornando como resultado un array con tres animales iguales.
 *
 *   - Si en el array hay un animal de cada el zorro se come a la gallina.
 *
 */

'use strict';

// NewAnimal, aqui la creamos

function newAnimal(type) {
    return {
        type,
        breed: function () {
            return this;
        },
    };
}

// Aqui creamos el zorro y la gallina

const Fox = newAnimal('zorro');
const Chicken = newAnimal('gallina');

// Aquí creamos un gonLunch 'BUSCAR BIEN LA DEFINICION DE GOLUNCH'

Fox.goLunch = function (animals) {
    let chikenIndex = null;

    for (let i = 0; i < animals.length; i++) {
        if (animals[i].type === 'gallina') {
            chikenIndex = i;
            break;
        }
    }

    animals.splice(chikenIndex, 1);

    return animals;
};

// Aquí generamos los animales

function generateAnimals(foxObj, chickenObj) {
    const animals = [];

    for (let i = 0; i < 2; i++) {
        const random = Math.floor(Math.random() * 2);

        if (random) {
            animals.push(foxObj);
        } else {
            animals.push(chickenObj);
        }
    }

    return animals;
}

// Aqui creamos la función que pide el ejercicio para resolverlo

function playGame(animals) {
    if (animals[0].type === animals[1].type) {
        const breed = animals[0].breed(); // Aquí creamos un nuevo animal si lo hubiera y hacemos un push
        animals.push(breed);
    } else {
        let foxIndex = 0;

        for (let i = 0; i < animals.length; i++) {
            if (animals[i].type === 'zorro') {
                foxIndex = i;
                break;
            }
        }

        animals[foxIndex].goLunch(animals);
    }

    return animals;
}

// ahora creamos una lista con animales

const animalList = generateAnimals(Fox, Chicken);

// Comenzamos el ejercicio

const result = playGame(animalList);

console.log(result);
