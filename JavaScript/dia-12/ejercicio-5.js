/**
 * #################
 * ## Ejercicio 5 ##
 * #################
 *
 * Dada la función "alphabetSoup(string)" tome el parámetro string y devuelve la cadena con las
 * letras en orden alfabético (es decir, "hola" se convierte en "ehllo").
 *
 */

'use strict';

function alphabetSoup(string) {
    let aux = string.split('');
    let auxOrdenado = aux.sort();
    return auxOrdenado.join('');
}
console.log(alphabetSoup('trabajo'));
