// para hacer expresiones regulares debemos:

const myRegex = /JavaScript/g; // la g es para hacerlo global y por si queremos cambiar todas
//las palabras de ese texto si no solo cambiara la primera. CON GI CAMBIA AUNQUE HAYA MAYUSCULAS O MINUSCULAS

const text = 'JavaScript mola mucho, pero es JavaScript es muy dificil';

console.log(myRegex.test(text));

console.log(text.replaceAll(myRegex, 'CSS'));

const mytext = /[A-z 0-9]/g; // asi eliminaria todo lo que ponemos ahi, recordar meterlo entre corchetes
// si usamos al principio ^ y asi lo negamos ! y eliminaria lo demás que no haya ahí
