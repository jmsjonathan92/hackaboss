'use strict';

const API = 'https://api.thecatapi.com/v1/images/search';

async function getCats(URL) {
    try {
        const response = await fetch(URL);

        const [cat] = await response.json();

        const div = document.querySelector('div.cat');

        const img = document.createElement('img');

        img.setAttribute('src', cat.url);
        img.setAttribute('alt', 'Un gatito');

        div.append(img);
    } catch (error) {
        console.log(error);
    }
}

getCats(API);
