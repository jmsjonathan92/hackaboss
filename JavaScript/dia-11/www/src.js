'use strict';
// metodo reviews
// ES IMPORTANTE HACER UN RETURN CON EL ACC SI NO VA A DAR UN ERROR

const numbers = [3, 5, 2, 10];

const total = numbers.reduce((acc, num /* acc = acumulator */) => {
    return (acc += num);
});

console.log(total);

// metodo some
// si metodos uno que este devolvera true y si no false

const fruits = ['manzana', 'pera', 'plátano', 'pera', 'naranja'];

const result = fruits.some((fruit) => {
    return fruit === 'naranja';
});

console.log(result);

// metodo every
// solo devolverá true si son todos iguales

const fruits2 = ['manzana', 'naranja'];

const result2 = fruits2.every((fruit) => {
    return fruit === 'naranja';
});

console.log(result2);

// metodo find
// metodo para encontrar un valor en concreto en la array, o el primer valor que encuentre

const numbers2 = [2, 4, 5, 8, 87, 23];

const result3 = numbers2.find((num) => {
    return num > 56;
});

console.log(result3);
