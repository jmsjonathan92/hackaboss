// utilizamos let y const para nombrar variables y const son variables constantes que no se pueden cambiar en n futuro.
// cuando no asignamos nada a un valor queda undefined, esta debe de marcarse sola y cuando no queremos meterle nada dentro pondremos null

'use strict';
/* 
console.log('Hola mundo');

let age = 10;

age = age + 5; // esto es equivalente a age += 5 se puede ponr suma resta o lo que sea

console.log(age); */

let mayorDeEdad = true;

// esto es por si solo hay dos condiciones 
if (mayorDeEdad) { // si se cumple la condición se ejecuta este bloque
    console.log('Mayor de edad');
} else { // si no se cumple se ejecuta este
    console.log('Menor de edad');
}



let animal = 200; // si pusieramos otro número saldría la útlima opción

// esto es por si hay mas condiciones 
/* if (animal === 0) {
    console.log('perro');
} else if (animal === 1) {
    console.log('gato');
} else if (animal === 2) {
    console.log('ratón');
} else {
    console.log('No se reconoce el animal');
} */


// esto es una alternativa al if else
switch (animal) {

    case 0: // en los casos podria poner perro gato o lo que quiera en vez de numeros
        console.log('perro');
        break; // es importante poner esto por que si no salta a la siguiente y la da como cierta

    case 1:
        console.log('gato');
        break;
    
    case 2:
        console.log('ratón');
        break;

    default:
        console.log('no se reconoce el animal');
}