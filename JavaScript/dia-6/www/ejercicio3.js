/* #################
 * ## Ejercicio 3 ##
 * #################
 *
 * Dados el siguiente array: [true, false, false, false, true, false, false, false, true]
 *
 *  - Los valores positivos son infectados.
 *
 *  - Los infectados transmiten su enfermedad a los no infectados que tienen a los lados.
 *
 * En este caso el resultado debería ser: [true, true, false, true, true, true, false, true, true];
 *
 */

'use strict';

const covid = [true, false, false, false, true, false, false, false, true];
const resultado = [];

for (let i = 0; i < covid.length; i++) {
    resultado[i] = false;
}

for (let i = 0; i < covid.length; i++) {
    if (covid[i] === true) {
        if (i - 1 >= 0) resultado[i - 1] = true;
        resultado[i] = true;
        if (i + 1 < covid.length) resultado[i + 1] = true;
    }
}

console.log(covid);
console.log(resultado);
