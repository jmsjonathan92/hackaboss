'use strict';

/* #################
 * ## Ejercicio 1 ##
 * #################
 *
 * Dado el array [3, 4, 13, 5, 6, 8], muestra por consola qué numeros son pares
 * y qué números son impares.
 *
 * Haz lo mismo pero en este caso indica qué números son primos y cuales no.
 *
 * Por último, crea un nuevo array en el que los valores sean el doble del array
 * original.
 *
 */

const nums = [3, 4, 13, 5, 6, 8];
const nums2 = [];

function isPrime(num) {
    for (let i = 2; i < num; i++) {
        if (num % i === 0) return false;
    }
    return true;
}

for (const num of nums) {
    if (num % 2 === 0) {
        console.warn(`El numero ${num} es par`);
    } else {
        console.error(`El numero ${num} es impar`);
    }
    if (isPrime(num)) {
        console.log(`El numero ${num} es primo`);
    } else isPrime(num);
    {
        console.log(`El numero ${num} no es primo`);
    }
    nums2.push(num * 2);
}

console.log(nums);
console.log(nums2);
