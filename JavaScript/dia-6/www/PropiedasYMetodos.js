'use strict';

// para comprobar la longitud de un string utilizamos esto: cuentan espacios y todo lo que tenga

const myString = '¡Métodos CON strings!';

console.log(myString.length);

// para pasar todas las letras a minusculas utilizamos:

console.log(myString.toLocaleLowerCase());

// para convertirlas en mayusculas:

console.log(myString.toUpperCase());

// para buscar un caracter o una serie de caracteres asi, te manda la ubicación del elemento, solo del primero:

console.log(myString.indexOf('s'));
console.log(myString.indexOf('w')); // si no existe te devuelve un -1
console.log(myString.indexOf('strings')); // te dice en la posición que empieza la palabra
console.log(myString.indexOf('s', 6)); // asi le decimos donde queramos que empiece a buscar

// este hace lo mismo pero busca de derecha a izquierda:

console.log(myString.lastIndexOf('s'));

// con este se puede repetir las veces que quieras:

console.log(myString.repeat5);

// con este metodo podemos sustituir las letras que queramos por otras, solo lo hará con la primera que encuentre desde la derecha

console.log(myString.replace('o', 'i'));

// también lo haría con palabras pero haría como uno nuevo:

console.log(myString.replace('strings', 'arrays')); // para que fuera original habría que crear una const nueva

// con esto creamos arrays con los elementos del string:

console.log(myString.split('')); // asi dividimos todo elemnto con arrays, si hacemos esto (' ') nos separará las palabras
// con ('/n') lo separaŕia en los saltos de linea
// con start y end nos indica la posición donde se encuentra el string, dónde empieza o dónde acaba con el numero de la linea.

// para saber que caracter hay en una posición es asi:
console.log(myString.charAt(3)); // si no hay ningún caracter en el espacio indicado nos aparecerá un espacio vacío

// con esto nos indicará con true o false si existe algún caracter
console.log(myString.includes('strings'));

// con esto nos devuelve un string nuevo elimnando los espacios en blanco:
console.log(myString.trim());
