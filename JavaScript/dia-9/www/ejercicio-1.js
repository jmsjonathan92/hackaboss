/* #################
 * ## Ejercicio 1 ##
 * #################
 *
 * Crea el objeto persona con las propiedades name y age. A mayores, crea un método que reciba un
 * array de animales y genere una nueva propiedad favoriteAnimals que almacene dicho array.
 *
 * Crea un segundo método que permita modificar cualquier propiedad del objeto. Este método debe
 * recibir dos argumentos, el nombre de la propiedad en formato string, y el valor que queremos
 * meter en la misma.
 *
 */

'use strict';

const person = {
    name: 'Laura',
    age: 35,
    getAnimals: function (animalList) {
        this.favoriteAnimals = animalList;
    },
    changeKey: function (key, value) {
        this[key] = value;
    },
};

person.getAnimals(['perro', 'gato']);
person.changeKey('age', 38);
person.changeKey('pet', { name: 'Pelusa', tupe: 'gato' });
person.age = 38;

console.log(person);
