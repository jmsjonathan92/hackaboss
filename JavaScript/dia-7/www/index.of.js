'use strict';

const fruits = ['manzana', 'pera', 'plátano', 'naranja', 'pera'];

// console.log(fruits.indexOf('pera', 4)); // aqui estamos buscando su posición y le indicamos dónde queramos que empiece a buscar

fruits.unshift('uva'); // con esto añadimos una y movemos las posiciones del resto +1

fruits.pop(); // con esto eliminamos el ultimo elemento sin importar cuál sea
// podriamos guardarlo haciendo asi:
const removedItem = fruits.pop();

// con esto podemos añadir elementos y indicar la posición donde queramos que empiece:
fruits.splice(1, 0, 'kiwi', 'sandia');

// con esto eliminamos el elemento pero del principio y cambian tambien todas las posiciones
//fruits.shift();
// tamnbién podremos alamcenarlo con el removedItem

// con esto dividimos todo en arraid y le damos la vuelta
let food = 'tortilla';
food = food.split('').reverse();
// con esto unimos los elementos de un arrayd:
food = food.join('');
// podriamos unirlo todo con el punto, .join('')

console.log(food);
console.log(fruits);
