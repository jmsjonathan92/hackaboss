/* #################
 * ## Ejercicio 4 ##
 * #################
 *
 * Crea un programa que compruebe si un string es palíndromo, es decir, que se lee igual
 * del derecho que del revés. Por ejemplo, "Arriba la birra" es un palíndromo.
 *
 */

'use strict';

const myText = 'Arriba la birra';

function reverseString(string) {
    return string.split('').reverse().join('');
}

// limpiar la cadena de texto y transformarlo a minúscula o mayuscula.
myText = myText.toLowerCase().replaceAll(' ', '');

// Creo una variable donde almaceno el string invertido
const reversedText = reserveString(myText);

// hago la comparación
console.log(myText === reversedText);
